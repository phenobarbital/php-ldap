<?php
class log {
	#instancia unica de la clase
	protected static $instance = null;
	#cache debug lines:
	protected static $_line = 0;
	#buffer de debug:
	protected static $_logbuffer = array();

	#nombre del archivo de log base
	protected static $_writer = null;

	#constructor de la clase
	protected function __construct() {}
	
	public function __destruct() {
		self::show();
	}

	#evitar que sea clonada accidentalmente la clase
	protected function __clone() {}

	/**
	 * GetInstance
	 * crea o accede a una instancia unica del Singleton
	 * @return object::instance
	 * @access public static
	 * @author Jesus Lara
	 */
	public static function getInstance() {
		if(empty(self::$instance)) {
			#crea una nueva instancia de la clase
			self::$instance = new self();
		}
		return self::$instance;
	}
	
	/**
	 * clean_log
	 * Clean and restart logs variables
	 */
	public static function clean() {
		self::$_line = 0;
		self::$_logbuffer = array();
	}

	 

	/**
	 * get_log
	 * retorna el log generado
	 * @return string
	 */
	public static function get() {
		$message = '';
		foreach(self::$_logbuffer as $k=>$v) {
			$msg = "{$k} : {$v}";
			$msg.= "\n";
			$message.= date('c') . '[' . time() . '] ' . $msg;
		}
		return $message;
	}

	/**
	 * show_log
	 * print log information to browser
	 */
	public static function show() {
		echo self::get();
		#para no mostrar 2 veces, se limpia el log al mostrarlo
		self::clean();
	}

	/**
	 * save_log
	 * write log to disk or elsewhere
	 * @access public static
	 * @return none
	 */
	public static function save() {
		return self::$_writer->write(self::get_log());
	}
	
	public static function write() {
		return self::save();
	}

	/**
	 * log
	 * add a log line to logbuffer
	 * @return none
	 * @access public static
	 * @author Jesus Lara
	 */
	public static function log($message) {
		self::$_line++;
		#save to the log buffer:
		self::$_logbuffer[self::$_line] = $message;
	}	
}
?>