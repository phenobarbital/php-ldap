#!/usr/bin/php
<?php
/**
 * script de add machine script:
 * en samba:
 * add machine script = addmachinemppef %h
 * Uso: el script de add machine script sustituye temporalmente el fallo del script smbldap-useradd para algunas estaciones de trabajo
 * @param machine (1): maquina desde donde el usuario ha iniciado sesion
 * @param ip (2): ip desde donde se conecta
 * @param arch (3): arquitectura del equipo
 * @uses Cada vez que un equipo se ingrese al dominio, es agregado al LDAP gracias a este script
 * @author Jesus Lara
 */
include dirname(dirname(__FILE__)) . "/conf/base.inc.php";
include_once BASE_DIR . "conf/include_ldap.inc.php";
if ($argc > 1) {
	$computer = $argv[1];
	$ip = $argv[2];
	$arch = $argv[3];

	log::log('Maquina:' . $computer);
	#ejecutar el smbldap-useradd
	$cmd = escapeshellcmd("/usr/sbin/smbldap-useradd -w '{$computer}'");
	@shell_exec($cmd);
	#nombre de maquina, adjunta un $
	$machine = $computer . '$';
	/*
	 #Active Directory
	 $ads = ldap::load('ads');
	 $ads->open();
	 #buscar la computadora:
	 $filter = "(&(objectClass=computer)(samaccountname={$machine}))";
	 $base = 'DC=MF,DC=gov,DC=ve';/usr/sbin/smbldap-useradd -w '{$machine}'
	 $entry = $ads->query($filter, $base, 'sub');
	 if ($entry->count()) {
		#si se encontró la maquina, entonces debemos tomar sus datos
		$sid = $entry->bin_to_str_sid('objectSid');
		$description = $entry->get_attribute('operatingSystem') . ' ' . $entry->get_attribute('operatingSystemVersion');
		$lastlogon = $entry->get_attribute('lastLogon');
		} else {
		$sid = 'S-1-5-21-1658329406-718221906-1845911597';
		$description = 'nuevo equipo';
		$lastlogon = 0;
		}
		#cerrar conexion a AD
		$ads->close();
		*/
	$sid = 'S-1-5-21-1658329406-718221906-1845911597';
	$description = 'Computadora ' . $arch;
	$lastlogon = 0;
	#Ahora procedemos a crear la cuenta de maquina
	log::log("SID computadora {$machine}: " . $sid);
	$filter = "(&(objectClass=posixAccount)(uid={$machine}))";
	$basemppef = 'ou=computadores,ou=samba,ou=servicios,dc=mppef,dc=gob,dc=ve';
	$mppef = ldap::load('mppef');
	if ($mppef->open()) {
		$entry = $mppef->query($filter, $basemppef);
		if ($entry->count()) {
			$uidNumber = $entry->uidNumber;
			$computerSID = $entry->sambaSID;
			#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
			#objeto unico dentro de todo el arbol LDAP
			ldap_schema::setAdapter($mppef);
			#construyo el arbol:
			ldap_schema::build();
			#Crear una computadora:
			$cp = $mppef->create($basemppef);
			#Agrego los objectclasses efectivos de una entrada:
			$cp->addObjectClass('account');
			$cp->addObjectClass('posixAccount');
			$cp->addObjectClass('sambaSamAccount');
			$cp->addObjectClass('shadowAccount');
			$cp->addObjectClass('ipHost');
			#atributo base de la entrada
			$cp->baseAttribute('uid');
			$cp->cn = $machine;
			$cp->sn = $machine;
			$cp->uid = $machine;
			$cp->setRDN('');
			$cp->o = 'MPPEF';
			$cp->sambaDomainName = 'MPPEF';
			$cp->sambaAcctFlags = '[W          ]';
			$cp->sambaPwdMustChange = '9223372036854775807';
			$cp->sambaPwdCanChange = '1237585718';
			$cp->sambaPrimaryGroupSID = 'S-1-5-21-1658329406-718221906-1845911597-515';
			#sid del equipo
			$cp->sambaSID = $computerSID;
			$cp->sambaLogonTime = $lastlogon;
			$cp->description = $description;
			//datos basicos:
			$cp->host = $computer;
			$cp->ipHostNumber = $ip;
			$cp->uidNumber = $uidNumber;
			$cp->gidNumber = 515;
			$cp->homeDirectory = '/dev/null';
			$cp->loginShell = '/bin/false';
			#agregamos
			$cp->update();
		}
		#busco una entrada DNS; si no existe, la crea:
		$filter = "(&(objectClass=dNSZone)(relativeDomainName={$computer}))";
		$basemppef = 'zoneName=mppef.gob.ve,ou=insite,ou=dns,ou=servicios,dc=mppef,dc=gob,dc=ve';
		$entry = $mppef->query($filter, $basemppef);
		if (!$entry->count()) { //si no existe la entrada:
			$new = true;
		} else {
			#si la entrada existe, actualizo el IP en DNS y en samba
			$new = false;
		}
		#creo una entrada en la base:
		$cp = $mppef->create($basemppef);
		$cp->addObjectClass('dNSZone');
		$cp->zoneName = 'mppef.gob.ve';
		$cp->baseAttribute('relativeDomainName');
		$cp->setRDN('');
		#nombre de maquina:
		$cp->relativeDomainName = $computer;
		$cp->dNSTTL = 3600;
		$cp->dNSClass = 'IN';
		#IP de la maquina:
		$cp->ARecord = $ip;
		#info de la maquina:
		$cp->HINFORecord = 'Equipo: ' .  $arch;
		if ($new) {
			$cp->insert();
		} else {
			$cp->update();
		}
		#cierro la conexion LDAP
		$mppef->close();
	}
}
return 1;
?>