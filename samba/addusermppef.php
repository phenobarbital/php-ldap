#!/usr/bin/php
<?php
include "../conf/base.inc.php";
include_once BASE_DIR . "conf/include_ldap.inc.php";
$mppef = ldap::load('mppef');
if ($mppef->open()) {
	#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
	#objeto unico dentro de todo el arbol LDAP
	ldap_schema::setAdapter($mppef);
	#construyo el arbol:
	ldap_schema::build();
	$mppef->close();
}
?>