#!/usr/bin/php
<?php
/*
 * instalar para usar:
 * aptitude install php5-snmp php5-mcrypt php5-curl php5-common php5-auth-pam php5-ldap php5-cli
 */
include dirname(dirname(__FILE__)) . "/conf/base.inc.php";
include_once BASE_DIR . "conf/include_ldap.inc.php";
/**
 * script de netlogon:
 * en samba:
 * root preexec = addmachinemppef %u %m
 * Uso: el script de netlogon se ejecuta CADA VEZ que un usuario inicia sesión en una maquina
 * @param usuario (1): nombre del usuario que inicia sesion
 * @param machine (2): maquina desde donde el usuario ha iniciado sesion
 * @param IP (3): IP de la maquina desde donde ha iniciado sesion
 * @param Arch (4): Arquitectura del equipo
 * @uses En caso de equipos que no haya procedido el script de addmachinescript, este agrega el equipo cuando es detectado
 * en el inicio de sesion
 * @author Jesus Lara
 * Variable 	Definition
 Tabla: Variables de Samba.
 Variable 	Definición
 Relativas a Clientes
 %a 	Arquitectura de Cliente (p.ej., Samba, WfWg, WinNT, Win95, o UNKNOWN)
 %I 	Dirección IP de Cliente (p.ej., 192.168.220.100)
 %m 	Nombre NetBIOS de Cliente
 %M 	Nombre DNS de Cliente
 Relativas a Usuarios
 %g 	Grupo Primario de %u
 %G 	Grupo Primario de %U
 %H 	Directorio "home" de %u
 %u 	Actual nombre usuario Unix
 %U 	Nombre de usuario (no siempre usado por Samba)
 Relativas a Recursos
 %p 	Automontador de ruta para el recurso, si difiere de %P
 %P 	Actual directorio root del recurso
 %S 	Actual nombre del recurso
 Relativas a Servidor
 %d 	Actual PID de servidor
 %h 	nombre DNS de máquina del servidor Samba
 %L 	Nombre NetBIOS del servidor Samba
 %N 	Directorio "home" del servidor, desde el mapa automount
 %v 	Versión de Samba
 Varias
 %R 	Nivel de protocolo SMB que se ha negociado
 %T 	Fecha y hora actual
 */
#si hay más de un argumento, debemos determinar si ese usuario existe:
if ($argc > 1) {
	#argumentos pasados al script
	#user
	$user = $argv[1];
	#Computer
	$computer = $argv[2];
	#IP
	$ip = $argv[3];
	#arch
	$arch = $argv[4];

	#necesito un log
	$file = new file();
	$file->filename('/var/log/samba/netlogon.log');
	$file->open('w');
	$file->write("Init logon | params:\n");
	foreach($argv as $a) {
		$file->write($a);
		#agregamos un salto de linea al final de cada entrada:
		$file->write("\n");
	}
	$file->write("\n");
	$file->write("Logon date: {date()}\n");
	$file->close();
	#directorio donde se crean los usuarios
	$dir = '/srv/samba/users/' . $user;
	#base de inserción y busqueda en mppef:
	$basemppef = 'ou=unidades,dc=mppef,dc=gob,dc=ve';
	$mppef = ldap::load('mppef');
	$filter = "(&(objectClass=posixAccount)(uid={$user}))";
	#abrir la conexion:
	$mppef->open();
	#si el usuario existe luego de un query:
	$u = $mppef->query($filter, $basemppef, 'sub');
	if ($u->uid == $user) {
		log::log("LDAP: encontrado uid: {$u->uid}");
		if ($u->sambaUserQuota) {
			$quota = $u->sambaUserQuota . 'm';
		} else {
			$quota = '1024m';
		}
		if (!is_dir($dir)) {
			#si no existe el directorio, entonces lo creamos, más todo lo demás
			log::log('netlogon: no existe el directorio ' . $dir);
			@mkdir($dir, 0775, true);
			#cambio la permisologia del directorio
			@chown($dir, $user);
			@chgrp($dir, 513);
		}
		#al usuario le asigno la cuota XFS respectiva
		$cmd = escapeshellcmd("/usr/sbin/xfs_quota -x -c 'limit bsoft={$quota} bhard={$quota} {$user}' /srv/samba/users");
		@shell_exec($cmd);
	}
	#luego, intento "crear" la entrada si no existe, de la maquina
	#ademas de sus entradas DNS
	#ejecutar el machineadd
	$cmd = escapeshellcmd("/usr/sbin/machineadd '{$computer}' '{$ip}' '{$arch}'");
	@shell_exec($cmd);
	#cerramos la conexion
	$mppef->close();
}
#imprimiendo el log:
#log::show();
?>