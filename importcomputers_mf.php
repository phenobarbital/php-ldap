#!/usr/bin/php
<?php
include "conf/base.inc.php";
include_once BASE_DIR . "conf/include_ldap.inc.php";

/*
 * Migrar computadoras segun la regla SAMBA
 * top
 * person
 * organizationalPerson
 * inetOrgPerson
 * posixAccount
 * sambaSamAccount
 *
 * description = Computer
 * uid = xproinf801185$
 * sambaSID = al que tenga en MF
 * gidNumber = 515
 * uidNumber = rid del SID
 * LMPassword = ''
 * NTPassword = 'E1EEE4AF9D53ABA9CD8B0AD4C759E58C'
 * sambaPrimaryGroupSID = S-1-5-21-1658329406-718221906-1845911597-515
 * sambaAcctFlagas = [W          ]
 *
 */

#inicio la conexion a ADS de ALUCASA
$base = 'DC=PRODUCCION,DC=com';
$ad = ldap::load('active');
$ad->open();

#inicio la conexion a LDAP de ALUCASA
$base_alucasa = 'cn=Computadoras,dc=alucasa,dc=com,dc=ve';
$alucasa = ldap::load('ldap');
$alucasa->open();
ldap_schema::setAdapter($alucasa);
#construyo el arbol:
ldap_schema::build();

$filter = "(&(objectClass=computer)(displayName=*))";
$entries = $ad->query($filter, $base, 'sub');
foreach($entries as $entry) {
	//var_dump($entries->row());
	$lastlogon = $entry->get_attribute('lastLogon');
	$l = $lastlogon;
	$l = date("Y", $entry->ADtoUnixTimestamp('lastLogon'));
	//	if ($l == 2010) {
	//		$entry->name;
	$fecha = date('d-m-Y', $entry->ADtoUnixTimestamp('lastLogon'));
	ldap::debug("Equipo: {$entry->name} ultimo arranque: {$fecha} \n");
	//	}
	# crear la computadora
	$cp = $alucasa->create($base_alucasa);
	#Agrego los objectclasses efectivos de una entrada:
	$cp->addObjectClass('organizationalPerson');
	$cp->addObjectClass('inetOrgPerson');
	$cp->addObjectClass('posixAccount');
	$cp->addObjectClass('shadowAccount');
	$cp->addObjectClass('sambaSamAccount');
	$cp->addObjectClass('ipHost');
	#atributo base de la entrada
	$cp->baseAttribute('cn');
	$cp->setRDN('');
	$cp->homeDirectory = '/dev/null';
	$cp->loginShell = '/bin/false';
	$cp->gecos = 'Computer ' . $entry->name;
	$cp->sn = $entry->name;
	$cp->gidNumber = 515;
	#sid del equipo
	$sid = $entry->bin_to_str_sid('objectSid');
	$cp->sambaSID = $sid;
	$cp->uidNumber = str_replace('S-1-5-21-89404532-1372478125-1516182889-', '', $sid);
	$cp->sambaDomainName = 'PRODUCCION.COM';
	$cp->displayName = $entry->displayName;
	$cp->description = 'Computer ' . $entry->name;
	//$cn = $entry->get_attribute('sAMAccountName');
	$cp->cn = strtolower($entry->name);
	$cp->uid = strtolower($entry->name);
	$cp->sambaPrimaryGroupSID = $sid . '-515';
	$cp->sambaAcctFlags = '[W          ]';
	$logon = $entry->get_attribute('lastLogon');
	if($logon) {
		$cp->sambaLogonTime = $logon;
	}
	$cp->description = $entry->get_attribute('operatingSystem') . ' ' . $entry->get_attribute('operatingSystemServicePack') . ' ' . $entry->get_attribute('operatingSystemVersion');
	#atributos del samba
	$cp->sambaPwdMustChange = '9223372036854775807';
	$cp->sambaLogoffTime = '2147483647';
	$cp->sambaKickoffTime = '2147483647';
	$cp->sambaPwdLastSet = $entry->get_attribute('pwdLastSet');
	$cp->sambaPwdCanChange = '1238728584';
	//$hash = new smbHash();
	//$passwd = $hash->lmhash('123456');
	$passwd = '';
	$cp->sambaNTPassword = $passwd;
	$cp->sambaLMPassword = $passwd;
	# informacion de IpHost
	$cp->ipHostNumber = '0.0.0.0';
	$cp->insert();
}

/*
 foreach($entries as $entry) {
 #Crear una computadora:
 $cp = $mppef->create($basemppef);
 #Agrego los objectclasses efectivos de una entrada:
 $cp->addObjectClass('organizationalPerson');
 $cp->addObjectClass('inetOrgPerson');
 $cp->addObjectClass('posixAccount');
 $cp->addObjectClass('sambaSamAccount');

 #atributo base de la entrada
 $cp->baseAttribute('uid');
 $cp->setRDN('');

 $cp->homeDirectory = '/dev/null';
 $cp->loginShell = '/bin/false';
 $cp->gecos = 'Computer';
 $cp->gidNumber = 515;
 #sid del equipo
 $cp->sambaSID = $entry->bin_to_str_sid('objectSid');
 $cp->sambaDomainName = 'MPPEF';
 $cp->displayName = $entry->displayName;
 $cp->description = 'Computer';
 //$cn = $entry->get_attribute('sAMAccountName');
 $cp->cn = strtolower($entry->name);
 $cp->uid = strtolower($entry->name);
 $cp->sambaPrimaryGroupSID = 'S-1-5-21-1658329406-718221906-1845911597-515';
 $cp->sambaAcctFlags = '[W          ]';
 $cp->sambaLogonTime = $entry->get_attribute('lastLogon');
 $cp->description = $entry->get_attribute('operatingSystem') . ' ' . $entry->get_attribute('operatingSystemVersion');
 #atributos del samba
 $cp->sambaPwdMustChange = '9223372036854775807';
 $cp->sambaLogoffTime = '2147483647';
 $cp->sambaKickoffTime = '2147483647';
 $cp->sambaPwdLastSet = '1238728584';
 $cp->sambaPwdCanChange = '1238728584';
 $cp->sambaNTPassword = '';
 //$cp->insert();
 }
 */
$ad->close();
$alucasa->close();
?>