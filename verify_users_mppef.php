#!/usr/bin/php
<?php
include_once 'ldap/ldap.inc.php';
include_once 'file/file.inc.php';

#carga del objeto ldap
ldap::init('conf/');

#base de inserción y busqueda en mppef:
$basemppef = 'ou=unidades,dc=mppef,dc=gob,dc=ve';
$mppef = ldap::load('mppef');

if ($mppef->open()) {
	#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
	#objeto unico dentro de todo el arbol LDAP
	ldap_schema::setAdapter($mppef);
	#construyo el arbol:
	ldap_schema::build();    
	#filtro de busqueda:
	$filter = "(&(objectClass=posixAccount)(uid=*))";
	#primero, verificamos las unidades del MF:
	$entries = $mppef->query($filter, $basemppef, 'sub');
	$uid = 1100;
        $error= 0;
	foreach($entries as $entry) {
            if (($entry->uid != 'elsanto') || ($entry->uid != 'jesuslara')) {
		#Crear una entrada de usuario
		$u = $mppef->create($basemppef);
		#Agrego los objectclasses efectivos de una entrada:
		$u->addObjectclass('organizationalPerson');
		$u->addObjectClass('inetOrgPerson');
		$u->addObjectClass('sambaSamAccount');
		$u->addObjectClass('qmailUser');
		$u->addObjectClass('posixAccount');
		$u->addObjectClass('shadowAccount');
		#atributo base de la entrada
		$u->baseAttribute('cn');
		#atributos base:
		$u->cn = $entry->cn;
		$u->sn = $entry->sn;
                #computar el uid
                $u->uidNumber = $uid;              
                #cuota generica de 1Gb para todos
                $u->mailQuotaSize = 1073741824;
                #incremento el uid en uno
                $uid++;
                #OU
                $u->o = 'MPPEF';
		#por ultimo, extraer la unidad funcional a la que pertenece:
		$a = ldap_explode_dn($entry->dn(), 1);
                //print_r($a);
		unset($a['count']);
                $ou = $a[2];
                $u->baseAttribute('cn');
                $u->ou = $ou;
		#RDN de la entrada:
		$u->setRDN("ou=usuarios,ou={$ou}");            
                #y guardo esta entrada
                if(!$u->update()) {
                    $error++;
                }
            }
        }
	echo "==== stats ====\n";
	echo 'Se obtuvieron ' . $entries->count() . " usuarios desde MPPEF.\n";
	echo "se iteraron los UID hasta el numero {$uid}\n";
        echo "ocurrieron {$error} errores durante el proceso \n";
	echo "==== ======\n";
        $mppef->close();
}
?>