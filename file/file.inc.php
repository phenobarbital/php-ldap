<?php
class file {
	#apuntador al archivo
	protected $file = null;
	
	#nombre del archivo
	protected $filename = '';
	
	public function filename($name) {
		$this->filename = $name;
	}
	
	public function open($mode = 'a+') {
		if($this->file = fopen($this->filename, $mode)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function close() {
		if ($this->file) {
			fclose($this->file);
		}
	}
	
	public function write($data) {
		if ($this->file) {
			if (fwrite($this->file, $data)=== TRUE) {
				return true;
			} else {
				return false;
			}
		}
	}
}
?>