#!/usr/bin/php
<?php
include_once 'ldap/ldap.inc.php';
include_once 'file/file.inc.php';

#carga del objeto ldap
ldap::init('conf/');

#base de busqueda en mf:
$basemf = 'OU=Unidades Funcionales,OU=Sede01,OU=MPPF,DC=mf,DC=gov,DC=ve';
$mf = ldap::load('ads');
$mf->open();

#filtro de busqueda:
$filter = "(&(objectClass=organizationalUnit)(&(!(ou=*Migrado*))(!(ou=Unity))(!(ou=Computador*))(!(ou=Impresora*))(!(ou=Impresoras))(!(ou=Usuarios))(!(ou=Grupos))))";
#primero, verificamos las unidades del MF:
$entries = $mf->query($filter, $basemf, 'base');
foreach($entries as $entry) {
	//print_r($entry->attributes());
	//print_r($entry->ou());
}

$mf->close();
?>