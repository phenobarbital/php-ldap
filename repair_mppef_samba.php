#!/usr/bin/php
<?php
include "conf/base.inc.php";
include_once BASE_DIR . "conf/include_ldap.inc.php";

#base de inserción y busqueda en mppef:
$basemppef = 'ou=unidades,dc=mppef,dc=gob,dc=ve';
$mppef = ldap::load('mppef');
#buscar el usuario dentro del ADS:
$ads = ldap::load('ads');
//$filter = "(&(objectClass=posixAccount)(uid=ejemplo))";
$filter = "(&(objectClass=posixAccount)(uid=*))";

#abrir la conexion:
$mppef->open();
$ads->open();

#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($mppef);
#construyo el arbol:
ldap_schema::build();

#obtenemos todos los usuarios:
$users = $mppef->query($filter, $basemppef, 'sub');
$excepciones = array('jesuslara', 'jrey', 'eagmunoz', 'jgonzalezg', 'hhalls', 'hespin', 'elsanto');
#inicializamos varias variables de control:
$editados = 0;
#gestion de errores
$error = 0;
$errores = array();

#control de la presencia en el ADS
$inads = 0;
$newuidnumber = 5000;

foreach($users as $user) {
	#Crear una entrada de usuario
	$u = $mppef->create($basemppef);
	#uid
	$uid = $user->uid;
	#Agrego los objectclasses efectivos de una entrada:
	$u->addObjectclass('organizationalPerson');
	$u->addObjectClass('inetOrgPerson');
	$u->addObjectClass('sambaSamAccount');
	$u->addObjectClass('qmailUser');
	$u->addObjectClass('posixAccount');
	$u->addObjectClass('shadowAccount');
	#atributo base de la entrada
	$u->baseAttribute('cn');
	#atributos base:
	$u->cn = $user->cn;
	$u->sn = $user->sn;
	#reparando entradas:
	$u->o = 'MPPEF';
	#corregir el GID del usuario
	$u->gidNumber = 513;
	#reparar atributos samba:
	$u->sambaPrimaryGroupSID = 513;
	$u->sambaDomainName = 'MPPEF';
	$u->sambaAcctFlags = '[U          ]';
	#reparar flags de samba:
	$u->sambaPasswordHistory = '0000000000000000000000000000000000000000000000000000000000000000';
	#cuota del filesystem de samba en MB
	$u->sambaUserQuota = 512;
	$entry = "(&(objectClass=user)(samaccounttype=". ADS_NORMAL_ACCOUNT .")(samaccountname={$uid}))";
	$base = 'DC=MF,DC=gov,DC=ve';
	$a = $ads->query($entry, $base, 'sub');
	if ($a->num_rows()) {
		$inads++;
		$sid = $a->bin_to_str_sid('objectSid');
		$rid = str_replace('S-1-5-21-1658329406-718221906-1845911597-', '', $sid);
		//echo "rid: {$rid}\n";
		$u->sambaSID = $sid;
		#si el UID no forma parte de las excepciones, normalizo su uidNumber
		if (!in_array($uid, $excepciones)) {
			$u->uidNumber = $rid;
		}
	} else {
		#si no se encuentra en usuario en el active directory, igual hay que normalizar
		if (!in_array($uid, $excepciones)) {
			$u->uidNumber = $newuidnumber;
			$u->sambaSID = 'S-1-5-21-1658329406-718221906-1845911597-' . $newuidnumber;
			$newuidnumber++;
		}
	}
	if(!$user->sambaNTPassword) {
		$u->sambaPwdLastSet = 10;
		$u->sambaPwdCanChange = 1238617258;
		$u->sambaPwdMustChange = 1238617259;
	}
	#definir si el password NT y LM no han sido establecidos:
	if (!$user->sambaNTPassword) {
		$u->sambaNTPassword = '259745CB123A52AA2E693AAACCA2DB52';
	}
	if (!$user->sambaLMPassword) {
		$u->sambaLMPassword = '0182BD0BD4444BF836077A718CCDF409';
	}
	#agregar si no tiene pager y mobile:
	if (!$user->mobile) {
		$u->mobile = 0;
	}
	if (!$user->pager) {
		$u->pager = 0;
	}
	#acomodar el mailhost:
	//$u->mailHost = 'correo.mppef.gob.ve';
	#tamaño máximo de un correo (20MB máximo):
	//$u->mailSizeMax = 20971520;
	#por ultimo, extraer la unidad funcional a la que pertenece:
	$a = ldap_explode_dn($user->dn(), 1);
	//print_r($a);
	unset($a['count']);
	$ou = $a[2];
	$u->baseAttribute('cn');
	$u->ou = $ou;
	#RDN de la entrada:
	$u->setRDN("ou=usuarios,ou={$ou}");
	#y guardo esta entrada
	if(!$u->update()) {
		$error++;
		$errores[$uid] = $u->dn();
	} else {
		echo $u->dn() . "\n";
		$editados++;
	}
}
echo "==== stats ====\n";
echo 'Se obtuvieron ' . $users->count() . " usuarios desde MPPEF.\n";
echo "se modificaron {$editados} usuarios existentes\n";
if ($inads) {
	echo "se encontraron {$inads} usuarios en el Active Directory\n";
}
if ($error) {
	echo "hubo {$error} errores de edicion; revisar log\n";
	var_dump($errores);
}
echo "Ultimo uid asignado a los usuario no ADS: {$newuidnumber}\n";

echo "==== ======\n";
#cerrar:
$mppef->close();
?>