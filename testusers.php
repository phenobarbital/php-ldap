#!/usr/bin/php
<?php
include_once 'ldap/ldap.inc.php';
include_once 'file/file.inc.php';

ldap::init('conf/');
$ldap = ldap::load('ads');

$ldap->open();
$base = 'OU=MPPF,DC=mf,DC=gov,DC=ve';
$filter = "(&(objectClass=user)(samaccounttype=". ADS_NORMAL_ACCOUNT .")(samaccountname=info07))";
$entry = $ldap->query($filter, $base, 'sub');
//itera sobre todas las entradas de tipo computadora y las guarda en un LDIF:
$file = new file();
$file->filename('ldifusersmf.ldif');
#abrimos el archivo:
$file->open('w');
foreach($entry as $v) {
	//echo $entry->cn(), ": ", $entry->bin_to_str_sid('objectSid'), "\n";
	$file->write($v->toLDIF());
	#agregamos un salto de linea al final de cada ldif:
	$file->write("\n");
}

#numero de usuarios:
echo "Existen : ", $entry->num_rows(), " usuarios.\n";

#cerramos el archivo
$file->close();

#y la conexion LDAP
$ldap->close();
?>