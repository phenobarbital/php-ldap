#!/usr/bin/php
<?php
include_once 'ldap/ldap.inc.php';
include_once 'file/file.inc.php';

#carga del objeto ldap
ldap::init('conf/');

#inicio la conexion a MF
$basemf = 'ou=MF,ou=Usuarios,dc=mf,dc=gob,dc=ve';
$mppf = ldap::load('mppf');
$mppf->open();

#base de inserción y busqueda en mppef:
$basemppef = 'ou=unidades,dc=mppef,dc=gob,dc=ve';
$mppef = ldap::load('mppef');
#creo mi archivo de log de usuarios malos:
$file = new file();
$file->filename('usersmppfbad.ldif');
$file->open('w');
if ($mppef->open()) {
	#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
	#objeto unico dentro de todo el arbol LDAP
	ldap_schema::setAdapter($mppef);
	#construyo el arbol:
	ldap_schema::build();
	#filtro de busqueda:
	$filter = "(&(objectClass=posixAccount)(uid=*))";
	#primero, verificamos las unidades del MF:
	$entries = $mppf->query($filter, $basemf, 'sub');
	$i = 0;
	#datos
	$nuevos = 0;
	$editados = 0;
	$error = 0;
	foreach($entries as $entry) {
		#si existe la entrada en mppef:
		$uid = $entry->uid;
		#Crear una entrada de usuario
		$u = $mppef->create($basemppef);
		#Agrego los objectclasses efectivos de una entrada:
		$u->addObjectclass('organizationalPerson');
		$u->addObjectClass('inetOrgPerson');
		$u->addObjectClass('sambaSamAccount');
		$u->addObjectClass('qmailUser');
		$u->addObjectClass('posixAccount');
		$u->addObjectClass('shadowAccount');
		#atributo base de la entrada
		$u->baseAttribute('cn');
		#atributos base:
		$u->cn = $entry->cn;
		$u->sn = $entry->sn;
		$givenName = $entry->givenName;
		if ($givenName) {
			$u->givenName = $givenName;
		}
		$u->uid = $entry->uid;
		#dato critico, su password:
		$u->userPassword = $entry->userPassword;
		#description:
		$desc = $entry->description;
		if ($desc) {
			$u->description = $desc;
		}
		#atributos que no cambian:
		$name = $entry->displayName;
		if ($name) {
			$u->displayName = $name;
		}
		$tlf = $entry->get_attribute('telephoneNumber');
		if ($tlf) {
			$u->telephoneNumber = $tlf;
		}
		$tlf = $entry->get_attribute('telexNumber');
		if ($tlf) {
			$u->telexNumber = $tlf;
		}
		$tlf = $entry->get_attribute('mobile');
		if ($tlf) {
			$u->mobile = $tlf;
		}
		$tlf = $entry->get_attribute('pager');
		if ($tlf) {
			$u->pager = $tlf;
		}
		$tlf = $entry->get_attribute('homePhone');
		if ($tlf) {
			$u->homePhone = $tlf;
		}
		#officeName:
		$u->physicalDeliveryOfficeName = $entry->physicalDeliveryOfficeName;
		$u->employeeType = $entry->employeeType;
		#datos posix:
		$u->uidNumber = $entry->uidNumber;
		$u->gidNumber = $entry->gidNumber;
		$u->homeDirectory = $entry->homeDirectory;
		$u->loginShell = $entry->loginShell;
		$u->gecos = $entry->gecos;
		#correo:
		#cuenta de correo mppef:
		$u->mail = "{$uid}@mppef.gob.ve";
		#la cuenta esta en dovecot:
		$u->mailHost = 'acacia.mppf.gob.ve';
		#direcciones de correo alternativas:
		$u->mailAlternateAddress = array("{$uid}@mf.gov.ve", "{$uid}@mppf.gob.ve", "{$uid}@mf.gob.ve");
		#atributos especificos de la entrada MPPEF:
		$u->accountStatus = 'active';
		$u->deliveryMode = 'virtual:';
		$u->mailQuotaSize = $entry->quota;
		#por ultimo, extraer la unidad funcional a la que pertenece:
		$a = ldap_explode_dn($entry->dn(), 1);
		unset($a['count']);
		$b = ldap_explode_dn($basemf, 1);
		unset($b['count']);
		$ou = array_diff($a, $b);
		$ou = $ou[1];
		#RDN de la entrada:
		$u->setRDN("ou=usuarios,ou={$ou}");
		print_r($u->getRDN());
		echo "\n";
		#busco si existe el usuario:
		$filter = "(&(objectClass=posixAccount)(uid={$uid}))";
		$e = $mppef->query($filter, $basemppef, 'sub');
		if (!$e->count()) {
			#usuario no existe;
			echo "importando a {$uid}\n";
			#domain sid
			$sid = 'S-1-5-21-1658329406-718221906-1845911597-';
			$u->sambaSID = $sid . $entry->uidNumber;
			echo "\n";
			print_r($u->row());
			echo "\n";
			if (!$u->insert()) {
				$file->write($entry->toLDIF());
				#agregamos un salto de linea al final de cada ldif:
				$file->write("\n");
				$error++;
			} else {
				$nuevos++;
			}
		} else {
			#usuario existe; debemos actualizar su entrada:
			echo "actualizando entradas de usuario {$uid}\n";
			#es necesario mantener el CN de la entrada original
			$u->cn = $e->cn;
			$u->sn = $e->sn;
			#verdadero OU de la entrada:
			$ou = $e->get_attribute('o');
			$u->setRDN("ou=usuarios,ou={$ou}");
			print_r($u->getRDN());
			echo "\n";
			print_r($u->row());
			echo "\n";
			if ($u->update()) {
				$editados++;
			} else {
				$file->write($entry->toLDIF());
				#agregamos un salto de linea al final de cada ldif:
				$file->write("\n");
				$error++;
			}
		}
	}
	echo "==== stats ====\n";
	echo 'Se obtuvieron ' . $entries->count() . " usuarios desde MPPF.\n";
	echo "se registraron {$nuevos} usuarios nuevos\n";
	echo "se modificaron {$editados} usuarios existentes\n";
	echo "hubo {$error} errores de insercion; revisar log\n";
	echo "==== ======\n";
	$mppf->close();
	$mppef->close();
}
#cerramos el archivo
$file->close();
?>