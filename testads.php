#!/usr/bin/php
<?php
include "conf/base.inc.php";
include_once BASE_DIR . "conf/include_ldap.inc.php";

$ldap = ldap::load('ads');

#nombre del usuario
$machine = $argv[1];
$user = $argv[2];
$ldap->open();



#buscar el usuario:
$entry = "(&(objectClass=user)(samaccounttype=". ADS_NORMAL_ACCOUNT .")(samaccountname={$user}))";
$base = 'DC=MF,DC=gov,DC=ve';
$entry = $ldap->query($entry, $base, 'sub');
log::log("SID usuario {$user}: " . $entry->bin_to_str_sid('objectSid'));

//$type = 805306369;
#buscar la computadora:
$entry = "(&(objectClass=computer)(samaccountname={$machine}))";
$base = 'DC=MF,DC=gov,DC=ve';
$entry = $ldap->query($entry, $base, 'sub');
log::log("SID computadora {$machine}: " . $entry->bin_to_str_sid('objectSid'));

$ldap->close();
?>