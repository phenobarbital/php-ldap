#!/usr/bin/php
<?php
include_once 'ldap/ldap.inc.php';
include_once 'file/file.inc.php';

#carga del objeto ldap
ldap::init('conf/');

#base de inserción y busqueda en mppef:
$basemppef = 'ou=unidades,dc=mppef,dc=gob,dc=ve';
$mppef = ldap::load('mppef');

//$mppf->open();
$mppef->open();
#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($mppef);
#construyo el arbol:
ldap_schema::build();


#ou de prueba:
$o = 'PRUEBA';

#crear una unidad organizacional en mppef:
$ou = $mppef->create($basemppef);
$ou->addObjectclass('organizationalUnit');
#RDN de la entrada:
$ou->setRDN(''); //es relativa a la base:
#atributo base de la entrada
$ou->baseAttribute('ou');
#atributos:
$ou->ou = $o;
$ou->physicalDeliveryOfficeName = $o;
$ou->description = $o;
$ou->l = 'sede01';
$ou->insert();

$ou->telexNumber = 1;
$ou->update();

#y luego la elimino:

//$ou->delete();

#cierro las conexiones
$mppef->close();

?>