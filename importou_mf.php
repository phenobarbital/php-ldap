#!/usr/bin/php
<?php
include "conf/base.inc.php";
include BASE_DIR . "conf/include_ldap.inc.php";

#carga del objeto ldap

#base de busqueda en alucasa:
$basead = 'DC=PRODUCCION,DC=com';
$ad = ldap::load('active');
$ad->open();

#base de inserción y busqueda en mppef:
$basealucasa = 'dc=alucasa,dc=com,dc=ve';
$alucasa = ldap::load('ldap');
$alucasa->open();

#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($alucasa);
#construyo el arbol:
ldap_schema::build();

#filtro de busqueda:
$filter = "(&(objectClass=organizationalUnit)(&(!(ou=*Migrado*))(!(ou=Unity))(!(ou=Computador*))(!(ou=Impresora*))(!(ou=Impresoras))(!(ou=Usuarios))(!(ou=Grupos))))";
$entries = $ad->query($filter, $basead, 'sub');
foreach($entries as $entry) {
	$o = $entry->get_attribute('ou');
	$o = str_replace(',', '', $o);
	#crear una unidad organizacional en mppef:
	$ou = $alucasa->create($basealucasa);
	$ou->addObjectclass('organizationalUnit');
	#RDN de la entrada:
	$ou->setRDN('');
	#atributo base de la entrada
	$ou->baseAttribute('ou');
	#atributos:
	$o = ucwords(strtolower($o));
	$ou->ou = $o;
	$ou->physicalDeliveryOfficeName = $o;
	$ou->description = $o;
	#row:
	print_r($ou->row());
	if ($ou->insert()) {
		echo "insertado : $ou->dn()\n";
		#creando grupos y usuarios de la entrada:
		#usuarios:
		$u = $alucasa->create("ou={$o},{$basealucasa}");
		$u->addObjectclass('namedObject');
		$u->baseAttribute('cn');
		$u->cn = 'alias';
		$u->insert();
		#grupos:
		$g = $alucasa->create("ou={$o},{$basealucasa}");
		$g->addObjectclass('namedObject');
		$g->baseAttribute('cn');
		$g->cn = 'impresoras';
		$g->insert();
	}
}

$ad->close();

/*
#inicio la conexion a MPPF
$basemf = 'ou=MF,ou=Usuarios,dc=mf,dc=gob,dc=ve';
$mppf = ldap::load('mppf');
$mppf->open();
$filter = "(objectClass=organizationalUnit)";
#buscar todas las OU
$entries = $mppf->query($filter, $basemf, 'sub');
foreach($entries as $entry) {
	$ou = $entry->ou;
	print_r($ou);
	echo "\n";
	$filtro = "(&(objectClass=organizationalUnit)(ou={$ou}))";
	#si existe la entrada en mppef:
	$e = $mppef->query($filtro, $basemppef, 'one');
	#si la entrada no existe:
	#debo crear la estructura en mppef de la OU:
	if (!$e->count()) {
		#crear una unidad organizacional en mppef:
		$o = $mppef->create($basemppef);
		$o->addObjectclass('organizationalUnit');
		#RDN de la entrada:
		$o->setRDN('');
		#atributo base de la entrada
		$o->baseAttribute('ou');
		#atributos:
		$o->ou = $ou;
		$o->physicalDeliveryOfficeName = $ou;
		$o->description = $ou;
		if ($o->insert()) {
			#creando grupos y usuarios de la entrada:
			#usuarios:
			$u = $mppef->create("ou={$ou},{$basemppef}");
			$u->addObjectclass('organizationalUnit');
			$u->baseAttribute('ou');
			$u->ou = 'usuarios';
			$u->insert();
			#grupos:
			$g = $mppef->create("ou={$ou},{$basemppef}");
			$g->addObjectclass('organizationalUnit');
			$g->baseAttribute('ou');
			$g->ou = 'grupos';
			$g->insert();
		}
	}
}

$mppf->close();
$mppef->close();
*/

$alucasa->close();
?>