#!/usr/bin/php
<?php
include "../conf/base.inc.php";
include BASE_DIR . "conf/include_ldap.inc.php";

$ldap = ldap::load('ldap');
$basealucasa = 'dc=alucasa,dc=com,dc=ve';

#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($ldap);
#construyo el arbol:
ldap_schema::build();

$i = 0;
if ($ldap->open()) {
	$lines = file('group');
	foreach ($lines as $line_num => $linea) {
		if (strlen($linea) > 1) {
			list($cn,$x,$gidnumber,$users) = explode(':', $linea);
			# creamos la definicion de grupo:
			#Crear un grupo
			$gid = $ldap->create($basealucasa);
			#Agrego los objectclasses efectivos de una entrada:
			$gid->addObjectclass('posixGroup');
			$gid->addObjectClass('sambaGroupMapping');
			#atributo base de la entrada
			$gid->baseAttribute('cn');
			#atributos que no cambian:
			$gid->cn = $cn;
			$gid->sambaGroupType = '2';
			$gid->sambaSID = 'S-1-5-21-89404532-1372478125-1516182889-' . $gidnumber;
			$gid->gidNumber = $gidnumber;
			$gid->setRDN("cn=Grupos");
			$usuarios = explode(',', $users);
			$miembros = array();
			foreach ($usuarios as $m) {
				$miembros[] = $m;
			}
			$gid->memberUid = $miembros;
			if(!$gid->insert()) {
				echo "Error cargando > " . $cn;
				$i++;
			} else {
				$gid->save();
			}
			
		}
	}
	echo "Hubo " . $i . " Errores";
	$ldap->close();
}
?>