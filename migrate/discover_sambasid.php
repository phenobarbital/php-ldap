#!/usr/bin/php
<?php
include "../conf/base.inc.php";
include_once BASE_DIR . "conf/include_ldap.inc.php";

$ldap = ldap::load('active');

#nombre del usuario
$user = 'administrator';

if ($ldap->open()) {
#buscar el usuario:
$entry = "(&(objectClass=user)(samaccounttype=". ADS_NORMAL_ACCOUNT .")(samaccountname={$user}))";
$base = 'DC=PRODUCCION,DC=com';
$entry = $ldap->query($entry, $base, 'sub');
log::log("SID usuario {$user}: " . $entry->bin_to_str_sid('objectSid') . "\n");
echo "SID usuario {$user}: " . $entry->bin_to_str_sid('objectSid')  . "\n";
$ldap->close();
}
?>