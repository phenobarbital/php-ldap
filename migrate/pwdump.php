#!/usr/bin/php
<?php
include "../conf/base.inc.php";
include BASE_DIR . "conf/include_ldap.inc.php";

$ldap = ldap::load('ldap');

#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($ldap);
#construyo el arbol:
ldap_schema::build();

$file = new file();
$file->filename('usuarios-faltantes.txt');
$file->open('w');

if ($ldap->open()) {
	$base = 'dc=alucasa,dc=com,dc=ve';
	$lines = file('claves.txt');
	$i = 0;
	$e = 0;
	$n = 0;
	foreach ($lines as $line_num => $linea) {
		list($login, $rid, $lmpwd, $ntpwd, $gecos, $homedir, $b) = explode(':', $linea);
		#filtro de busqueda:
		$login = strtolower(str_replace('$','', $login));
		$filter = "(uid={$login})";
		$entry = $ldap->query($filter, $base, 'sub');
		var_dump($entry->dn());
		if ($entry->dn()!="") {
			$entry->baseAttribute('cn');
			$entry->userPassword = "{LANMAN}" . strtoupper($lmpwd);
			$entry->sambaNTPassword = strtoupper($ntpwd);
			$entry->sambaLMPassword = strtoupper($lmpwd);
			if (!$entry->save()) {
				$e++;
				$file->write("error : " . $linea);
				$file->write("\n");
			} else {
				$n++;
			}
		} else {
			$file->write("ausente : " . $linea);
			$file->write("\n");
			$i++;
		}
	}
	echo 'Se corrigieron ' . $n . " usuarios.\n";
	echo "No se encontraron {$i} entradas, se encontraron $e errores\n";
	#cerramos el archivo
	$file->close();
	$ldap->close();
}
?>
