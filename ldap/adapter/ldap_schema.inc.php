<?php
/**
 *
 * ldap_schema
 * clase que contiene todos los schemas asociados al LDAP actual
 * @package ldap
 * @version 0.1.1
 * @static
 **/
class ldap_schema {
	#instancia de la clase singleton
	protected static $instance = null;

	#informacion del LDAP
	protected static $base = '';

	#arreglo de entradas basico
	protected static $metadata = array();

	#conexion activa (adapter) al LDAP
	protected static $db = null;

	#cantidad de objectclasses en el servidor
	protected static $count = 0;

	#nombres de todos los objectclasses en el LDAP
	protected static $name = array();

	#array contenedor de objectclasses
	protected static $objectclass = array();

	#define si el LDAP ha sido parseado en objectclasses
	protected static $loaded = false;


	/**
	 * getInstance
	 * Singleton method for return class
	 *
	 * @return tdbo_ldap_schema
	 */
	public static function getInstance() {
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * setAdapter
	 * Define el adaptador de conexion para el objeto schema actual
	 * @param tdbo $db
	 */
	public static function setAdapter(ldap_adapter $db) {
		if (self::$db == null) { //si no ha sido definido por defecto;
			if ($db) {
				if (get_parent_class($db) == 'ldap_adapter') {
					self::$db = $db;
					if ($db->is_connected() == false) {
						#abro la conexion (autoconecto)
						$db->open();
					}
					return self::$db->is_connected();
				} else {
					$msg = "LDAP schema Error: Tipo de Adaptador invalido en: ";
					ldap::debug($msg .  __METHOD__);
				}
			} else {
				$msg = "LDAP schema Error: Debe especificar un objeto de conexion (adapter_ldap) valido para conectarse en : ";
				ldap::debug($msg .  __METHOD__);
			}
		}
		return $this;
	}

	/**
	 * db
	 * retorna el conector activo
	 * @return tdbo_adapter_ldap
	 */
	public static function db() {
		if (self::$db) {
			return self::$db;
		} else {
			return false;
		}
	}

	/**
	 * clear
	 * Limpia el arreglo de variables de metadatos
	 * @return void
	 *
	 */
	public static function clear() {
		self::$count = 0;
		self::$metadata = array();
		self::$name = array();
		self::$objectclass = array();
		self::$loaded = false;
	}

	#construyo el arbol de objectclasses LDAP a partir de la base indicada:
	/**
	* build
	* Construir el arreglo de metadatos del servidor LDAP
	*
	*/
	public static function build() {
		$base = self::$db->base_dn();
		if (($base!='') && (self::$base != $base)) {
			#limpio el arreglo de nombres, objectclasses y metadatos
			self::clear();
			#me he cambiado a otro server
			self::$base = $base;
			self::get_schemas(self::$base);
			#hemos construido el arbol LDAP del servidor actual
			self::$loaded = true;
		}
	}

	public static function is_loaded() {
		return self::$loaded;
	}

	protected static function get_schemas($base) {
		#trato de obtener el subSchema para luego buscar los esquemas activos del servidor:
		$search = @ldap_read(self::db()->connection(), $base,'objectClass=*',array('subschemaSubentry'));
		$entries = @ldap_get_entries(self::db()->connection(),$search);
		$entry = $entries[0];
		$sub_schema_sub_entry = isset($entry[0]) ? $entry[0] : false;
		$schema_dn = isset($entry[$sub_schema_sub_entry][0]) ? $entry[$sub_schema_sub_entry][0] : false;
		#obtengo la lista de todos los schemas (objectclasses) en el servidor:
		$schema_search = @ldap_read(self::db()->connection(),$schema_dn,'(objectClass=subschema)',array('objectclasses'),0,0,0,LDAP_DEREF_ALWAYS);
		$schema_entries = @ldap_get_entries(self::db()->connection(),$schema_search);
		self::$metadata = $schema_entries[0]['objectclasses'];
		self::$count = self::$metadata['count'];
		unset(self::$metadata['count']);
		foreach(self::$metadata as $class) {
			self::add_objectclass($class);
		}
	}

	//TODO: mejorar la expresion regular
	protected static function add_objectclass($class_string) {
		$a = array();
		#oid:
		$exp = "/\( (.*)\s(?=NAME)/";
		$oid = self::get_item($exp, $class_string);
		if (strpos($class_string, 'DESC')) {
			$exp = '/NAME\s(.*)\s(?=DESC)/';
			$name = self::get_name($exp, $class_string);
		} else {
			#debo buscar por SUP
			$exp = '/NAME\s(.*)\s(?=SUP)/';
			if (!$name = self::get_item($exp, $class_string)) {
				#debo buscar STRUCTURAL
				$exp = '/NAME\s(.*)\s(?=STRUCTURAL)/';
				if (!$name = self::get_item($exp, $class_string)) {
					$exp = '/NAME\s(.*)\s(?=AUXILIARY)/';
					if (!$name = self::get_item($exp, $class_string)) {
						#error
						ldap::debug("LDAP schema: No se como encontrar el nombre de ese objectclass $class_string");
						return false;
					}
				}
			}
		}
		self::$name[] = $name;
		$obj = new ldap_objectclass($oid, $name);
		#es auxiliar?
		if (strpos($class_string, 'AUXILIARY')) {
			$obj->auxiliary = true;
		} else {
			$obj->auxiliary = false;
		}
		$obj->structural= !$obj->auxiliary;
		#es abstracto?
		if (strpos($class_string, 'ABSTRACT')) {
			$obj->abstract = true;
		} else {
			$obj->abstract = false;
		}
		#descripcion del atributo
		$exp = "/DESC\s\'(.*)\'\ ?(SUP)/";
		$obj->description = self::get_item($exp, $class_string);
		#padre del atributo:
		$exp = "/SUP\s(.*)\s\ ?(STRUCTURAL|AUXILIARY)/";
		$obj->superior = self::get_array_items($exp, $class_string);
		#campos MUST:
		if (strpos($class_string, 'MAY')) {
			$exp = "/MUST\s(.*)\ ?(?=MAY)/";
		} else {
			$exp = "/MUST\s(.*)/";
		}
		$obj->setMust(self::get_array_items($exp, $class_string));
		#campos MAY:
		if (strpos($class_string, 'X-NDS_CONTAINMENT')) {
			$exp = "/MAY\s(.*)\sX-NDS_CONTAINMENT(.*)/";
		} else {
			$exp = "/MAY\s(.*)/";
		}
		$obj->setMay(self::get_array_items($exp, $class_string));
		#agrego el objectclass:
		self::$objectclass[$name] = $obj;
	}

	public function get_name($exp, $string) {
		$a = array();
		if (preg_match($exp, $string, $a)) {
			$str = $a[1];
			#elimino las comas y parentesis:
			if ($str != '') {
				$str = trim(strtr($str, array(')'=>'', '('=>'', "'"=>'')));
				if(stripos($str, ' ')!==false) {
					$a = explode(' ', $str);
					$str = $a[0];
				}
				return $str;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	#items que son escalares (DESC, oid, etc)
	public function get_item($exp, $string) {
		$a = array();
		if (preg_match($exp, $string, $a)) {
			$str = $a[1];
			#elimino las comas y parentesis:
			if ($str != '') {
				$str = trim(strtr($str, array(')'=>'', '('=>'', "'"=>'')));
				return $str;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	#items que son arreglos (MUST, MAY, etc)
	public function get_array_items($exp, $string) {
		$a = array();
		if (preg_match($exp, $string, $a)) {
			$str = $a[1];
			#elimino las comas y parentesis:
			if ($str != '') {
				$str = trim(strtr($str, array(')'=>'', '('=>'', "'"=>'')));
				if (strpos($str, ' $ ')) {
					return explode(' $ ', $str);
				} else {
					return explode(' ', $str);
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	#metodos informativos de la clase:
	public static function exists($schema) {
		if (in_array($schema, self::$name)) {
			return true;
		} else {
			return false;
		}
	}

	public static function get_fields($schema) {
		if (self::exists($schema)) {
			return self::$objectclass[$schema]->fields;
		}
	}

	public static function get_names() {
		return self::$name;
	}

	/**
	 * get_objectclass
	 * retorna un objeto ldap_objectclass
	 * @param string $schema
	 * @return ldap_objectclass
	 */
	public static function get_objectclass($schema) {
		if (self::exists($schema)) {
			return self::$objectclass[$schema];
		}
	}

	#esta definido en array de objectclasses
	public static function is_defined() {

	}

}

class ldap_objectclass {
	public $oid = '';
	public $name = '';
	public $description = '';
	public $auxiliary = false;
	public $abstract = false;
	public $structural = true;
	public $superior = array();
	public $must = array();
	public $may = array();
	public $fields = array();

	public function __construct($oid, $name) {
		$this->oid = $oid;
		$this->name = $name;
	}

	public function name() {
		return $this->name;
	}

	public function setMust($a) {
		$this->must = $a;
		$this->fillFields($a);
	}

	public function must() {
		return $this->must;
	}

	public function setMay($a) {
		$this->may = $a;
		$this->fillFields($a);
	}

	public function may() {
		$this->may;
	}

	protected function fillFields($a) {
		if (is_array($a)) {
			$this->fields = array_merge($this->fields, $a);
		}
	}

	public function fields() {
		return $this->fields;
	}

	public function isPK($field) {
		if (is_array($this->must)) {
			if (in_array($field, $this->must)) {
				return true;
			} else {
				return false;
			}
		}
	}

	#un alias de campos
	public function attributes() {
		return $this->fields;
	}

	#retorna el padre superior de este objectclass
	public function sup() {
		return $this->superior;
	}
}
?>