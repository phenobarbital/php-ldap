<?php
class ldap_result Implements iterator,countable, ArrayAccess {
	#contenedor en bruto de las entradas:
	protected $entries = null;
	protected $entry = null;

	#objeto ldap_util
	protected $util = null;

	#contenedor de la fila de datos:
	protected $row = null;
	protected $_modified = array();

	#domain name base:
	public $_base_dn = '';
	protected $_connection = null;
	protected $query = null;

	# -- atributos especiales de LDAP
	protected $_dn = '';
	protected $_ufn = '';
	protected $_entryDN = '';
	protected $_entryUUID = '';
	protected $_createTimestamp = '';
	protected $_modifyTimestamp = '';
	protected $_hasSubordinates = false;
	#un array conteniendo todos los atributos de la entrada
	protected $_attributes = array();
	protected $_newattrs = array();

	#un array q contiene todos los nombres de objectclasses de la entrada
	protected $_objectclasses = array();
	#un array que contiene los objectclasses de la entrada
	protected $objectclass = array();

	# arreglo de atributos protegidos
	protected $protectedattrs = array('structuralObjectClass', 'entryUUID', 'entryCSN', 'modifiersName', 'modifyTimestamp', 'hasSubordinates', 'entryDN', 'subschemaSubentry', 'createTimestamp', 'creatorsName');

	#propiedades de los iteradores
	protected $_count =  null;
	protected $_seek = 0;

	# -- atributos de las operaciones de crear, modificar y borrar
	#define el atributo base
	protected $baseattr = 'cn';
	#define en DN base:
	protected $base = '';
	#define el RDN base
	protected $rdn = '';

	#atributos requeridos de la entrada
	protected $_pk = array();


	public function __construct($connection, $query, $dn) {
		$this->_base_dn = $dn;
		$this->_connection = $connection;
		$this->query = $query;
		#util
		$this->util = ldap_util::getInstance();
		#fila de datos:
		$this->row = null;
	}

	#retorna el BASE DN usado en las consultas
	public function base_dn() {
		return $this->_base_dn;
	}

	public function setBaseDN($dn) {
		$this->_base_dn = $dn;
	}

	public function free() {
		if ($this->query) {
			@ldap_free_result($this->query);
			$this->query = null;
		}
		#llevo a reset a atributos especiales
		$this->_dn = '';
		$this->_ufn = '';
		$this->_entryDN = '';
		$this->_entryUUID = '';
		$this->_createTimestamp = '';
		$this->_modifyTimestamp = '';
		$this->_hasSubordinates = false;
		$this->_objectclasses = array();
		$this->_attributes = array();
		$this->_entries = null;
		$this->query = null;
	}

	/**
	 * entries
	 * retorna las entradas como un array
	 * @return array $entries
	 */
	public function entries() {
		return $this->_entries;
	}

	public function objectClasses() {
		return $this->_objectclasses;
	}

	// funciones informativas
	#devuelve completo el array de atributos
	public function attributes() {
		return $this->_attributes;
	}

	public function values() {
		return array_values($this->row);
	}

	#atributos especiales del ldap
	public function dn() {
		return $this->_dn;
	}

	#retorna el DN relativo (sin el DN absoluto)
	public function relativeDN() {
		return str_replace(',' . $this->_base_dn, '', $this->_dn);
	}

	public function explode_dn($with_attrs = 0) {
		$arr = ldap_explode_dn($this->_dn, $with_attrs ? 1 : 0);
		unset($arr['count']);
		return $arr;
	}

	#funciones de conversion:
	#conversion de una fecha ldap a timestamp regular:
	public function LDAPTimestamp($ldaptimestamp) {
		$match = array();
		$timestamp = '';
		//$matchstring = "#(d{4})(d{2})(d{2})(d{2})(d{2})(d{2})Z#";
		$matchstring = "#([0-9]{4})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})Z#";
		if(preg_match($matchstring,$ldaptimestamp,$match)) {
			$timestamp = mktime($match[4], $match[5], $match[6], $match[2], $match[3], $match[1]);
			//$timestamp = gmmktime($match['4'], $match['5'], $match['6'], $match['2'], $match['3'], $match['1']);
		}
		return $timestamp;
	}

	public function ufn() {
		return $this->_ufn;
	}
	public function cn() {
		$cn = $this->get_attribute('cn');
		if (is_array($cn)) {
			return $cn[0];
		} else {
			return false;
		}
	}

	public function uid() {
		$uid = $this->get_attribute('uid');
		if (is_array($uid)) {
			return $uid[0];
		} else {
			return false;
		}
	}

	public function ou() {
		$ou = $this->get_attribute('ou');
		if (is_array($ou)) {
			return $ou[0];
		} else {
			return false;
		}
	}

	public function uuid() {
		return $this->_entryUUID;
	}

	public function creator() {
		$creator = $this->get_attribute('creatorsName');
		if (is_array($creator)) {
			return $creator[0];
		} else {
			return $creator;
		}
	}

	/**
	 * exists
	 * indica si existe la entrada
	 *
	 * @return boolean
	 */
	public function exists($attribute) {
		$attr = strtolower($attribute);
		if (in_array($attr, $this->_attributes) || in_array($attribute, $this->_attributes)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * hasChildrens
	 * indica si una entrada posee nodos (leafs) hijos
	 *
	 * @return boolean
	 */
	public function hasChildrens() {
		$childs = $this->get_attribute('hasSubordinates');
		if ($childs[0] == 'TRUE') {
			return true;
		} else {
			return false;
		}
	}

	public function get_attribute($nombre_attr) {
		if (in_array($nombre_attr, $this->_attributes)) {
			if (isset($this->row[$nombre_attr])) {
				#retorna el array del atributo
				if (count($this->row[$nombre_attr])==1) {
					return $this->row[$nombre_attr][0];
				} else {
					return $this->row[$nombre_attr];
				}
			} else {
				# atributo no definido
				return false;
			}
		} else {
			return false;
		}
	}

	public function set_attribute($nombre_attr, $value) {
		if (in_array($nombre_attr, $this->_attributes)) {
			$this->row[$nombre_attr] = $value;
		} else {
			#no se encuentra en el array de atributos
			array_push($this->_attributes, $nombre_attr);
			$this->row[$nombre_attr] = $value;
			# lo agrego a la lista de nuevos atributos
			$this->_newattrs[$nombre_attr] = $value;
			return true;
		}
	}

	# --- metodos para operar las filas de datos del LDAP

	public function row() {
		return $this->row;
	}

	public function set_row($row) {
		//$this->_attributes = ldap_get_attributes($this->_connection, $this->_data);
		$this->row = ldap_get_attributes($this->_connection, $row);
		$this->_dn = ldap_get_dn($this->_connection, $row);
		$this->_ufn = ldap_dn2ufn($this->_dn);
		$n = $this->row['count'];
		#obtengo todos los atributos de una entrada
		//ldap::debug("tenemos $n atributos");
		$this->_attributes = array();
		for ($i=0; $i < $n; $i++) {
			$attribute = $this->row[$i];
			$this->_attributes[] = $attribute;
			unset($this->row[$i]);
			unset($this->row[$attribute]['count']);
		}
		$this->_attributes = array_unique($this->_attributes);
		#obtengo los object-class:
		$this->_objectclasses = $this->row['objectClass'];
		if (is_array($this->_objectclasses)) {
			#retiro el count de los objectclasses
			unset($this->_objectclasses['count']);
		}
		//var_dump($this->_objectclasses);
		#eliminemos por si acaso el objectGUID:
		unset($this->row['objectGUID']);
		unset($this->_attributes['objectGUID']);
		#y eliminamos el contador:
		unset($this->row['count']);
		//print_r($this->_attributes);
		//print_r($this->row);
	}

	# --- metodos iteradores

	public function first() {
		$this->_seek = 0;
		if ($this->entry = ldap_first_entry($this->_connection, $this->query)) {
			$this->set_row($this->entry);
		}
	}

	// -- metodos iteradores
	public function rewind() {
		$this->first();
	}

	public function reset() {
		#hay resultados en la busqueda
		$this->_count = ldap_count_entries($this->_connection, $this->query);
		ldap::debug("* LDAP: se encontraron {$this->_count} entradas");
		#creo el primer row
		$this->first();
	}

	public function next() {
		$this->entry = @ldap_next_entry($this->_connection, $this->entry);
		$this->_seek++;
		if ($this->entry) {
			$this->set_row($this->entry);
			return true;
		} else {
			return false;
		}
	}

	#iterator::valid
	public function valid() {
		if (($this->_seek < $this->_count) == true) {
			return true;
		} else {
			return false;
		}
	}

	#retorna la fila actual
	public function current() {
		return $this;
	}

	#retorna el offset de la entrada
	public function key() {
		return $this->_seek;
	}

	#Countable::count
	public function count() {
		return $this->_count;
	}

	public function num_rows() {
		return $this->_count;
	}

	# --- operador array access

	# -- interfaz arrayAccess
	public function offsetExists($offset) {
		if($this->exists($offset)) {
			return true;
		} else {
			return false;
		}
	}

	public function offsetGet($offset) {
		if($this->exists($offset)) {
			return $this->get_attribute($offset);
		} else {
			return false;
		}
	}

	public function offsetSet($offset, $value) {
		if($this->exists($offset)) {
			//$this->get($offset)->setValue($value);
		} else {
			return false;
		}
	}

	#pasa a null un campo
	public function offsetUnset($offset) {
		if($this->exists($offset)) {
			unset($this->row[$offset]);
			unset($this->_attributes[$offset]);
		} else {
			return false;
		}
	}

	# --- operadores magicos

	public function __get($field) {
		if ($this->exists($field)) {
			if (count($this->row[$field]) == 1) {
				return $this->row[$field][0];
			} else {
				return $this->row[$field];
			}
		} else {
			return false;
		}
	}

	public function __isset($field) {
		return $this->exists($field);
	}

	public function __unset($field) {
		$this->offsetUnset($field);
	}

	#funcion magica call
	public function __call($method, $args) {
		if (count($args)) {
			$value = $this->get_attribute($args[0]);
			if (is_array($value)) {
				$value = $value[0];
			}
			return $this->util->$method($value);
		}
	}

	public function objectClass() {
		return $this->_objectclasses;
	}

	#funciones interesantes de LDAP
	public function toLDIF() {
		$str = "dn : {$this->dn()}\n";
		foreach($this->attributes() as $a) {
			$v = $this->get_attribute($a);
			var_dump($v);
			if (is_array($v)) {
				foreach($v as $value) {
					switch($a) {
						case 'objectSid':
						case 'mS-DS-CreatorSID':
							$val = $this->util->bin_to_str_sid($value);
							break;
						case 'objectGUID':
							//$val = $this->util->str_to_hex_guid($value);
							//es un dato hexa innecesario
							continue;
							break;
						default:
							$val = $value;
					}
					$str.= "{$a}: {$val} \n";
				}
			} else {
				$str.= "{$a}: {$v} \n";
			}
		}
		#retorno la entrada exportada como LDIF
		return $str;
	}

	# --- operaciones de creacion, modificacion o eliminacion de entradas
	/**
	* Defino el RDN de la entrada
	*
	* @param string $rdn
	*/
	public function setRDN($rdn = '') {
		if ($rdn!='') {
			$this->rdn = $rdn;
		}
	}

	public function getRDN() {
		return $this->rdn;
	}

	/**
	 * define el atributo base de la entrada (sn, cn)
	 *
	 * @param string $attr
	 */
	public function baseAttribute($attr = 'cn') {
		if (is_scalar($attr) && $attr!='') {
			$this->baseattr = $attr;
		} else {
			$this->baseattr = $this->_pk[0];
		}
	}

	public function getbaseAttribute() {
		return $this->baseattr;
	}

	/**
	 * Permite Agregar el (los) objectclass(es) asociado(s) a una entrada
	 *
	 * @param string $class
	 */
	public function addObjectClass($schema) {
		if (ldap_schema::exists($schema)) {
			$class = ldap_schema::get_objectclass($schema);
			$sup = $class->sup();
			#si posee objectclasses hijos, itero sobre ellos:
			if ($sup) {
				foreach($sup as $s) {
					$this->addObjectClass($s);
				}
			}
			#nombre del objectclass:
			$name = $class->name();
			#agrego el class actual al grupo de objectclasses de la entrada
			$this->objectclass[$name] = $class;
			#y a la lista de nombres de objectclasses:
			$this->_objectclasses[] = $name;
			#construyo la lista de atributos:
			#construyo la lista de campos, lista de requeridos, creo una fila de fields para entrada, etc
			foreach($class->fields() as $field) {
				$this->addAttribute($field, $class);
			}
			if(isset($this->_pk[0])) {
				#defino quien el es atributo base (siempre el primero):
				$this->baseattr = $this->_pk[0];
			}
			#simplifico la lista de objectclasses
			$this->_objectclasses = array_unique($this->_objectclasses);
		} else {
			ldap::debug("LDAP entry: No existe el objectclass definido en este servidor");
			return false;
		}
	}

	/**
	 * addAttribute
	 * Permite agregar un atributo a la lista de atributos disponibles
	 * @param string $name
	 * @param ldap_objectclass $class
	 */
	protected function addAttribute($name, ldap_objectclass $class) {
		if ($class->isPK($name) && $name!='objectClass') {
			#indico que el campo es unico:
			array_push($this->_pk, $name);
		}
		array_push($this->_attributes, $name);
	}

	#retorna un arreglo con los PK de la fila
	public function pk() {
		return $this->_pk;
	}

	#retorna el valor del campo id, si son varios, un array
	public function id() {
		$pk = $this->_pk;
		$data = array();
		foreach($pk as $k) {
			$data[$k] = $this->row[$k];
		}
		return $data;
	}

	#retorna un array conteniendo el (los) PK y sus valores
	public function getId($old = false) {
		return $this->id($old);
	}

	#metodo magico para definir el valor de un campo
	public function __set($field, $value) {
		array_push($this->_modified, $field);
		$this->set_attribute($field, $value);
	}

	public function insert() {
		#preparo la entrada:
		if ($this->prepare_entry()) {
			//echo $this->base . "\n";
			//print_r($this->objectClasses());
			$result = @ldap_add($this->_connection, $this->base, $this->row);
			if ($result) {
				return true;
			} else {
				$error = ldap_error($this->_connection);
				ldap::debug("LDAP insert error: No se ha podido insertar la entrada {$this->base}; error: {$error}");
				return false;
			}
		}
	}

	protected function prepare_entry() {
		#adjunto los objectclasses de la entrada:
		foreach($this->_objectclasses as $objectclass) {
			if (!isset($this->row['ObjectClass']) || !is_array($this->row['ObjectClass']) || !in_array($objectclass, $this->row['ObjectClass'])){
				$this->row['ObjectClass'][] = $objectclass;
			}
		}
		#creo el DN de la entrada:
		var_dump($this->rdn);
		if ($this->rdn != '') {
			$this->base = $this->rdn . ',' .  $this->base_dn();
		} else {
			$this->base = $this->base_dn();
		}
		#agrego el base attribute en base a su valor:
		if (isset($this->row[$this->baseattr])) {
			//print_r($this->row);
			if (is_array($this->row[$this->baseattr])) {
				$u = $this->row[$this->baseattr][0];
			} else {
				$u = $this->row[$this->baseattr];
			}
			$this->base = "{$this->baseattr}={$u}," . $this->base;
		} else {
			ldap::debug('LDAP Entry error: No se puede agregar la entrada; falta el valor para la entrada requerida base');
		}
		#existe el dn?
		if (!$this->dn_exists($this->base)) {
			return true;
		} else {
			//ldap::debug("LDAP insert error: No se ha podido insertar la entrada {$this->base}, la entrada ya existe!");
			return false; //la entrada ya existe
		}
		//echo $this->base;
	}

	#determina si un DN ya existe en el arbol LDAP
	public function dn_exists($dn) {
		$n = @ldap_list($this->_connection, $dn, $this->base_dn());
		if ($n) {
			return true;
		} else {
			return false;
		}
	}

	#permite borrar una entrada LDAP a partir de su DN
	public function delete($dn = '') {
		$this->prepare_entry();
		$result = ldap_delete($this->_connection, $dn);
		if ($result) {
			return true;
		} else {
			return false;
		}
	}

	public function update() {
		#preparo la entrada:
		$this->prepare_entry();
		$fila = array();
		foreach ($this->row as $key=>$attr) {
			if(!in_array($key, $this->protectedattrs)) {
				$fila[$key] = $attr;
				if(in_array($key, $this->_newattrs)) {
					//var_dump($attr);
				}
			}
		}
		# agrego los nuevos atributos
		foreach ($this->_newattrs as $key=>$attr) {
			if(!in_array($key, $this->_attributes)) {
				$entry[$key] = $attr;
				ldap_mod_add($this->_connection, $this->base, $entry);
			}
		}
		# edito la entrada
		$result = ldap_modify($this->_connection, $this->base, $fila);
		$entry = array();
		#y modifico los existentes:
		foreach($this->row as $key=>$attr) {
			if(!in_array($key, $this->protectedattrs)) {
				$entry[$key]=$attr;
				ldap_mod_replace($this->_connection, $this->base, $entry);
			}
		}
		if ($result) {
			return true;
		}
	}

	# permite editar solamente atributos de una entrada que han sido modificados
	public function save() {
		# obtengo el DN
		$dn = $this->dn();
		//var_dump($dn);
		#preparo la entrada:
		$this->prepare_entry();
		if ($dn) {
			# agrego los nuevos atributos
			foreach ($this->_newattrs as $key=>$attr) {
				if(!in_array($key, $this->_attributes)) {
					$entry[$key] = $attr;
					ldap_mod_add($this->_connection, $dn, $entry);
				}
			}
			$this->_modified = array_unique($this->_modified);
			foreach ($this->row as $key=>$attr) {
				if(in_array($key, $this->_modified)) {
					$entry[$key]=$attr;
					if(!ldap_mod_replace($this->_connection, $dn, $entry)) {
						if (defined('DEBUG') && DEBUG == true) {
							ldap::debug('LDAP Error: No se ha podido modificar la entrada ' . $this->dn());
						}
					}
				}
			}
		}
		return true;
	}

	public function copy() {

	}

	public function move() {

	}
}
?>