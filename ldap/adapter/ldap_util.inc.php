<?php
class ldap_util {
	protected static $instance = null;
	#objeto que valida el contenido de un campo:
	protected $_validate = null;
	#encoding:
	protected $_encoding = 'UTF-8';

	#evitar la clonacion de la clase:
	protected function __construct() {
		#defino la zona
		date_default_timezone_set('America/Caracas');
	}

	protected function __clone() {}

	#GetInstance, crea o accede a una instancia unica del Singleton
	public static function getInstance() {
		if(empty(self::$instance)) {
			#crea una nueva instancia de la clase
			self::$instance = new self();
		}
		return self::$instance;
	}

	# -- funciones utilitarias
	public function toString($val, $opt_separator = ',') {
		if ($opt_separator == '') {
			$opt_separator = ',';
		}
		if (is_array($val)) {
			return implode($opt_separator, $val);
		} else {
			return $val;
		}
	}

	public function split($val, $opt_separator = ' ') {
		if ($opt_separator == '') {
			$opt_separator = ' ';
		}
		if (is_scalar($val)) {
			return explode($opt_separator, $val);
		}
	}

	#retorna un tipo convertido a fecha
	public function LDAPtoDate($val, $format = 'd/m/Y') {
		$matchstring = "#([0-9]{4})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})Z#";
		if(preg_match($matchstring,$val,$match)) {
			#es un timestamp de LDAP
			$timestamp = mktime($match[4], $match[5], $match[6], $match[2], $match[3], $match[1]);
		} else {
			#es un timestamp regular
			$timestamp = $val;
		}
		$date = date($format, $timestamp);
		if ($date) {
			return $date;
		} else {
			return false;
		}
	}

	public function filetimeToTimestamp ($filetime) {
		$win_secs = substr($filetime,0,strlen($filetime)-7); // divide by 10 000 000 to get seconds
		$unix_timestamp = ($win_secs - 11644473600); // 1.1.1600 -> 1.1.1970 difference in seconds
		return $unix_timestamp;
	}

	public function filetimeToDate($filetime) {
		$val = $this->filetimeToTimestamp($filetime);
		return strftime("%Y-%m-%d %H:%M:%S", $val);
	}

	public function ansiToTimestamp($val) {
		#forma ANSI
		$matchstring = "/^(19[0-9][0-9]|20[0-9][0-9])\-(0[1-9]|1[012])\-(0[1-9]|[12][1-9]|3[01])$/";
		if(preg_match($matchstring,$val,$match)) {
			return mktime($match[4], $match[5], $match[6], $match[2], $match[3], $match[1]);
		} else {
			return false;
		}
	}

	public function toDate($val, $format = "d/m/Y") {
		$timestamp = '';
		#forma Latina
		$matchstring = "/^(0[1-9]|[12][1-9]|3[01])\/(0[1-9]|1[012])\/(19[0-9][0-9]|20[0-9][0-9])$/";
		if(preg_match($matchstring,$val,$match)) {
			$timestamp = mktime(0, 0, 0, $match[2], $match[3], $match[1]);
		}
		$matchstring = "/^(19[0-9][0-9]|20[0-9][0-9])\-(0[1-9]|1[012])\-(0[1-9]|[12][1-9]|3[01])$/";
		if(preg_match($matchstring,$val,$match)) {
			$timestamp = mktime(0, 0, 0, $match[2], $match[3], $match[1]);
		}
		#defino el formato de conversion
		if ($format == '') {
			$format = "d/m/Y";
		}
		if ($timestamp) {
			return date($format, $timestamp);
		} else {
			return false;
		}
	}

	public function toLongDate($val) {
		return $this->toDate($val, "D M j G:i:s T Y");
	}

	public function timestampToAnsi($val) {
		return strftime("%Y-%m-%d %H:%M:%S", $val);
	}

	public function AnsiToDate($val, $format = "d/m/Y") {
		$matchstring = "/^(19[0-9][0-9]|20[0-9][0-9])\-(0[1-9]|1[012])\-(0[1-9]|[12][1-9]|3[01])$/";
		if(preg_match($matchstring,$val,$match)) {
			$timestamp = mktime(0, 0, 0, $match[2], $match[3], $match[1]);
		} else {
			$timestamp = strtotime($val);
		}
		#defino el formato de conversion
		if ($format == '') {
			$format = "d/m/Y";
		}
		return date($format, $timestamp);
	}

	public function LDAPtoLongDate($val) {
		return $this->LDAPtoDate($val, "D M j G:i:s T Y");
	}

	public function LDAPtoAnsiDate($val) {
		return $this->LDAPtoDate($val, "Y-m-d H:i:s");
	}

	public function ADtoUnixTimestamp($val) {
		// nano seconds (yes, nano seconds) since jan 1st 1601
		$secsAfterADEpoch = $val / (10000000); // seconds since jan 1st 1601
		// unix epoch - AD epoch * number of tropical days * seconds in a day
		$ADToUnixConvertor = ((1970-1601) * 365.242190) * 86400;
		// unix Timestamp version of AD timestamp
		$unixTsLastLogon = intval($secsAfterADEpoch - $ADToUnixConvertor);
		return $unixTsLastLogon;
	}

	public function toArray($val, $separator = ',') {
		if (is_string($val)) {
			return explode($separator, $val);
		}
	}

	public function capitalize($val) {
		return ucwords(strtolower($val));
	}

	public function toMD5($val) {
		if (is_string($val)) {
			return md5($val);
		} else {
			return false;
		}
	}

	//TODO: hacer un metodo que devuelva la version correcta del SHA1, MD5 y Chyper de un registro LDAP
	public function fromMD5($val) {
		return str_replace('{MD5}', '', $val);
	}

	// -- retorno de valores
	public function quotedValue($val) {
		if (is_numeric($val) || is_float($val)) {
			if($val === 0) {
				$quoted_value = '0';
			} else {
				$quoted_value = $val;
			}
		} else {
			if ($val === '') {
				$quoted_value = "''";
			} else {
				$quoted_value = "'{$val}'";
			}
		}
		return $quoted_value;
	}

	#retorna el valor "escapado"
	public function escaped_value($val) {
		$escaped_value = htmlspecialchars($val, ENT_QUOTES, $this->_encoding);
		return $escaped_value;
	}

	# --- funciones para determinar el tipo de un valor:
	public function isDate($val) {
		return $this->_validate->valid_date($val);
	}

	public function isString($val) {
		if (is_string($val)) {
			return true;
		} else {
			return false;
		}
	}

	public function isArray($val) {
		return is_array($val);
	}

	public static function padzero($val, $num_zeros = 5) {
		return str_pad($val, 5, "0", STR_PAD_LEFT);
	}

	#funcion rapida para la limpieza de variables:
	public static function sanitize($val) {
		$cleaned = preg_replace("/[^{_}a-zA-Z0-9]/", "", $val);
		return $cleaned;
	}

	#ejecuta una suma de verificacion (checksum)
	public function checksum($val) {
		return crc32($val);
	}

	#funciones de ADS
	// This function will convert a string GUID value into a HEX value to search AD.
	public function str_to_hex_guid($str_guid) {
		$str_guid = str_replace('-', '', $str_guid);
		$octet_str = substr($str_guid, 6, 2);
		$octet_str .= substr($str_guid, 4, 2);
		$octet_str .= substr($str_guid, 2, 2);
		$octet_str .= substr($str_guid, 0, 2);
		$octet_str .= substr($str_guid, 10, 2);
		$octet_str .= substr($str_guid, 8, 2);
		$octet_str .= substr($str_guid, 14, 2);
		$octet_str .= substr($str_guid, 12, 2);
		$octet_str .= substr($str_guid, 16, strlen($str_guid));
		return $octet_str;
	}

	// Returns the textual SID
	public function bin_to_str_sid($binsid) {
		$hex_sid = bin2hex($binsid);
		//$hex_sid = self::b2h($binsid);
		//print_r($hex_sid);
		// Get revision-part of SID
		$rev = hexdec(substr($hex_sid, 0, 2));
		// Get count of sub-auth
		$subcount = hexdec(substr($hex_sid, 2, 2));
		// SECURITY_NT_AUTHORITY
		$auth = hexdec(substr($hex_sid, 4, 12));
		$result  = "$rev-$auth";
		for ($x=0;$x < $subcount; $x++) {
			$subauth[$x] = hexdec(self::little_endian(substr($hex_sid, 16 + ($x * 8), 8)));
			$result .= "-" . $subauth[$x];
		}
		// Cheat by tacking on the S-
		return 'S-' . $result;
	}

	function b2h($str) {
		$hex = dechex(bindec($str));
		return $hex;
	}

	function unibin2hex($u) {
		$k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
		$k1 = bin2hex(substr($k, 0, 1));
		$k2 = bin2hex(substr($k, 1, 1));
		return $k2.$k1;
	}

	// Converts a little-endian hex-number to one, that 'hexdec' can convert
	function little_endian($hex) {
		$result = "";
		for ($x = strlen($hex) - 2; $x >= 0; $x = $x - 2) {
			$result .= substr($hex, $x, 2);
		}
		return $result;
	}

	// swap little-endian to big-endian
	function flip_endian($str) {
		// make sure #digits is even
		if ( strlen($str) & 1 ) {
			$str = '0' . $str;
		}
		$t = '';
		for ($i = strlen($str)-2; $i >= 0; $i-=2) {
			$t .= substr($str, $i, 2);
		}
		return $t;
	}


	// This function will convert a binary value guid into a valid string.
	function bin_to_str_guid($object_guid) {
		$hex_guid = bin2hex($object_guid);
		$hex_guid_to_guid_str = '';
		for($k = 1; $k <= 4; ++$k) {
			$hex_guid_to_guid_str .= substr($hex_guid, 8 - 2 * $k, 2);
		}
		$hex_guid_to_guid_str .= '-';
		for($k = 1; $k <= 2; ++$k) {
			$hex_guid_to_guid_str .= substr($hex_guid, 12 - 2 * $k, 2);
		}
		$hex_guid_to_guid_str .= '-';
		for($k = 1; $k <= 2; ++$k) {
			$hex_guid_to_guid_str .= substr($hex_guid, 16 - 2 * $k, 2);
		}
		$hex_guid_to_guid_str .= '-' . substr($hex_guid, 16, 4);
		$hex_guid_to_guid_str .= '-' . substr($hex_guid, 20);

		return strtoupper($hex_guid_to_guid_str);
	}
}
?>