<?php
class openldap extends ldap_adapter {

	#operaciones de insercion, actualizacion o borrado
	protected $rdn = '';

	# Permite la conexion a un recurso de LDAP
	public final function open() {
		if (defined('DEBUG') && DEBUG == true) {
			ldap::debug('LDAP: Iniciando conexion');
		}
		if (!$this->_connected) {
			if ($this->use_ssl == true) {
				$this->_connection = @ldap_connect('ldaps://' . $this->_host . ":{$this->_port}");
			} else {
				$this->_connection = @ldap_connect('ldap://' . $this->_host . ":{$this->_port}");
			}
			if (defined('DEBUG') && DEBUG == true) {
				ldap::debug('LDAP: Ejecutando conexion');
			}
			#haz pasado los parametros, entonces iniciar la conexion
			if ($this->_connection) {
				ldap::debug("LDAP: conectandose al LDAP host {$this->_host} usando el puerto {$this->_port}");
				foreach(self::$_options as $opt => $value) {
					$constant = constant($opt);
					ldap_set_option($this->_connection, $constant, $value);
				}
				$this->_auth = ldap_bind($this->_connection, $this->_username, $this->_password);
				if ($this->_auth) {
					$this->_connected = true;
					ldap::debug("LDAP: Autenticado con {$this->_username}");
				} else {
					$msg = "LDAP: Error autenticandose contra servidor ldap {$this->_host} usando el usuario {$this->_username}";
					ldap::debug($msg);
					$this->_connected = false;
				}
			} else {
				$msg = "LDAP: Error conectandose al servidor ldap {$this->_base_dn} en el host {$this->_host} usando el usuario {$this->_username}";
				ldap::debug($msg);
				$this->_connected = false;
			}
		}
		return $this->_connected;
	}

	# Permite la desconexion de un recurso de base de datos
	public function close(){
		if (!empty($this->_connection)) {
			@ldap_unbind($this->_connection);
			$this->_connection = null;
		}
		ldap::debug("LDAP: cerrando conexion al LDAP {$this->_base_dn} en {$this->_host}");
		$this->_connected = false;
	}

	#funcion para cambiar el usuario por defecto
	public function select_user($user, $passwd) {
		return ldap_bind($this->_connection, $user, $passwd);
	}

	public function query($filter, $base = '', $scope = '', $attrs = null) {
		if ($this->_connected) {
			#defino la base de busqueda:
			if ($base) {
				$base_dn = $base;
			} else {
				$base_dn = $this->_base_dn;
			}
			#como se tratan los alias
			$deref = LDAP_DEREF_NEVER;
			#defino los atributos a encontrar de cada registro
			if (is_array($this->_onlyneed)) {
				$attributes = $this->_onlyneed;
			} elseif(!is_null($attrs)) {
				$attributes = $attrs;
			} else {
				$attributes = array("*","+");
			}
			if ($scope != '') {
				$this->_scope = $scope;
			} elseif($this->_scope =='') {
				$this->_scope = 'sub';
			}
			//ldap::debug('ADS Query: '. $filter . ', base dn: ' . $base_dn);
			switch ($this->_scope) {
				case 'one':
					#realizo la busqueda por nivel base:
					$query = ldap_read($this->_connection, $base_dn, $filter, $attributes, null, $this->_sizelimit, $this->_timelimit);
					break;
				case 'base':
					#realizo la busqueda en un unico nivel:
					$query = ldap_list($this->_connection, $base_dn, $filter, $attributes, null, $this->_sizelimit, $this->_timelimit);
					break;
				case 'sub':
				default:
					#realizo la busqueda por subtree:
					$query = ldap_search($this->_connection, $base_dn, $filter, $attributes, null, 0, 0);
					break;
			}
			if ($query) {
				$result = new ldap_result($this->_connection, $query, $base_dn);
				$result->reset();
				return $result;
			} else {
				$err = ldap_error($this->_connection);
				$msg = "openLDAP: query error {$err} en :";
				ldap::debug($msg .  __METHOD__);
				return false;
			}
		}else {
			return false;
		}
	}

	public function create($basedn = '') {
		$result = new ldap_result($this->_connection, null, $basedn);
		return $result;
	}

}
?>