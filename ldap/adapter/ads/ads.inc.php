<?php
// Different type of accounts in AD
define ('ADS_NORMAL_ACCOUNT', 805306368);
define ('ADS_WORKSTATION_TRUST', 805306369);
define ('ADS_INTERDOMAIN_TRUST', 805306370);
define ('ADS_SECURITY_GLOBAL_GROUP', 268435456);
define ('ADS_DISTRIBUTION_GROUP', 268435457);
define ('ADS_SECURITY_LOCAL_GROUP', 536870912);
define ('ADS_DISTRIBUTION_LOCAL_GROUP', 536870913);

class ads extends ldap_adapter {
	#domain name base:
	public $_base_dn = '';
	public $_netbios = '';
	public $_domain = '';

	public function __construct($config) {
		parent::__construct($config);
		$this->_domain = $config['domain'];
		$this->_netbios = $config['netbios_name'];
	}

	# Permite la conexion a un recurso de LDAP
	public final function open() {
		if (!$this->_connected) {
			if ($this->use_ssl == true) {
				$this->_connection = @ldap_connect('ldaps://' . $this->_host);
			} else {
				$this->_connection = @ldap_connect('ldap://' . $this->_host);
			}
			#haz pasado los parametros, entonces iniciar la conexion
			if ($this->_connection) {
				ldap::debug("ADS: conectandose al LDAP host {$this->_host} usando el puerto {$this->_port}");
				foreach(self::$_options as $opt => $value) {
					$constant = constant($opt);
					ldap_set_option($this->_connection, $constant, $value);
				}
				$name = $this->_netbios . '\\' . $this->_username;
				$this->_auth = @ldap_bind($this->_connection, $name, $this->_password);
				if ($this->_auth) {
					$this->_connected = true;
					ldap::debug("ADS: Autenticado con {$this->_username}");
				} else {
					$msg = "ADS: Error autenticandose contra servidor ADS {$this->_host} usando el usuario {$this->_username}";
					ldap::debug($msg);
					$this->_connected = false;
				}
			} else {
				$msg = "ADS: Error conectandose al servidor ldap {$this->_base_dn} en el host {$this->_host} usando el usuario {$this->_username}";
				$this->_connected = false;
			}
		}
		return $this->_connected;
	}

	# Permite la desconexion de un recurso de base de datos
	public function close(){
		if (!empty($this->_connection)) {
			@ldap_unbind($this->_connection);
			$this->_connection = null;
		}
		ldap::debug("ADS: cerrando conexion al ADS {$this->_base_dn} en {$this->_host}");
		$this->_connected = false;
	}

	#funcion para cambiar el usuario por defecto
	public function select_user($user, $passwd) {
		$name = $this->_netbios . '\\' . $user;
		return ldap_bind($this->_connection, $name, $passwd);
	}

	public function query($filter, $base = '', $scope = '', $attrs = null) {
		if ($this->_connected) {
			#defino la base de busqueda:
			if ($base) {
				$base_dn = $base;
			} else {
				$base_dn = $this->_base_dn;
			}
			#como se tratan los alias
			$deref = LDAP_DEREF_NEVER;
			#defino los atributos a encontrar de cada registro
			if (is_array($this->_onlyneed)) {
				$attributes = $this->_onlyneed;
			} elseif(!is_null($attrs)) {
				$attributes = $attrs;
			} else {
				$attributes = array("*","+");
			}
			if ($scope != '') {
				$this->_scope = $scope;
			} elseif($this->_scope =='') {
				$this->_scope = 'sub';
			}
			//ldap::debug('ADS Query: '. $filter . ', base dn: ' . $base_dn);
			switch ($this->_scope) {
				case 'one':
					#realizo la busqueda por nivel base:
					$query = ldap_read($this->_connection, $base_dn, $filter, $attributes);
					break;
				case 'base':
					#realizo la busqueda en un unico nivel:
					$query = ldap_list($this->_connection, $base_dn, $filter, $attributes);
					break;
				case 'sub':
				default:
					#realizo la busqueda por subtree:
					$query = ldap_search($this->_connection, $base_dn, $filter, $attributes, null, $this->_sizelimit, $this->_timelimit);
					break;
			}
			if ($query) {
				$result = new ldap_result($this->_connection, $query, $base_dn);
				$result->reset();
				return $result;
			} else {
				$err = ldap_error($this->_connection);
				$msg = "ADS: query error {$err} en :";
				ldap::debug($msg .  __METHOD__);
				return false;
			}
		}else {
			return false;
		}
	}

}
?>