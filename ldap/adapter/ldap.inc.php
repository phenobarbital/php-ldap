<?php
class ldap_adapter {
	#informacion de configuracion de la base de datos
	public $_config = array();

	public $use_ssl = false;

	#parametros de conexion
	protected static $_options = array();

	#variables de conexion
	protected $_connection = null;
	protected $_connected = false;

	#domain name base:
	public $_base_dn = '';
	protected $_username = '';
	protected $_host = 'localhost';
	protected $_password = '';
	protected $_port = 389;

	#busqueda:
	protected $_objectClass = '(objectClass=*)';
	#busqueda de esquemas
	protected $_schemaSearch = '(objectClass=Subschema)';
	protected $_base_filter = "(objectclass=*)";

	#autenticacion
	protected $_auth = null;

	#cosas que necesito:
	protected $_onlyneed = null;

	protected $_sizelimit = 0;

	protected $_timelimit = 0;

	#ambito por defecto de las busquedas
	protected $_scope = 'sub';

	#sentencia que genera la ultima consulta
	public $_sentence = '';

	#contenedor del query realizado:
	protected $_query = null;

	#contiene la fila de datos
	protected $_rowset = null;

	#contenedor del objeto util
	protected $util = null;

	protected $_errorMessages = array(
	0x00 => "LDAP_SUCCESS",
	0x01 => "LDAP_OPERATIONS_ERROR",
	0x02 => "LDAP_PROTOCOL_ERROR",
	0x03 => "LDAP_TIMELIMIT_EXCEEDED",
	0x04 => "LDAP_SIZELIMIT_EXCEEDED",
	0x05 => "LDAP_COMPARE_FALSE",
	0x06 => "LDAP_COMPARE_TRUE",
	0x07 => "LDAP_AUTH_METHOD_NOT_SUPPORTED",
	0x08 => "LDAP_STRONG_AUTH_REQUIRED",
	0x09 => "LDAP_PARTIAL_RESULTS",
	0x0a => "LDAP_REFERRAL",
	0x0b => "LDAP_ADMINLIMIT_EXCEEDED",
	0x0c => "LDAP_UNAVAILABLE_CRITICAL_EXTENSION",
	0x0d => "LDAP_CONFIDENTIALITY_REQUIRED",
	0x0e => "LDAP_SASL_BIND_INPROGRESS",
	0x10 => "LDAP_NO_SUCH_ATTRIBUTE",
	0x11 => "LDAP_UNDEFINED_TYPE",
	0x12 => "LDAP_INAPPROPRIATE_MATCHING",
	0x13 => "LDAP_CONSTRAINT_VIOLATION",
	0x14 => "LDAP_TYPE_OR_VALUE_EXISTS",
	0x15 => "LDAP_INVALID_SYNTAX",
	0x20 => "LDAP_NO_SUCH_OBJECT",
	0x21 => "LDAP_ALIAS_PROBLEM",
	0x22 => "LDAP_INVALID_DN_SYNTAX",
	0x23 => "LDAP_IS_LEAF",
	0x24 => "LDAP_ALIAS_DEREF_PROBLEM",
	0x30 => "LDAP_INAPPROPRIATE_AUTH",
	0x31 => "LDAP_INVALID_CREDENTIALS",
	0x32 => "LDAP_INSUFFICIENT_ACCESS",
	0x33 => "LDAP_BUSY",
	0x34 => "LDAP_UNAVAILABLE",
	0x35 => "LDAP_UNWILLING_TO_PERFORM",
	0x36 => "LDAP_LOOP_DETECT",
	0x3C => "LDAP_SORT_CONTROL_MISSING",
	0x3D => "LDAP_INDEX_RANGE_ERROR",
	0x40 => "LDAP_NAMING_VIOLATION",
	0x41 => "LDAP_OBJECT_CLASS_VIOLATION",
	0x42 => "LDAP_NOT_ALLOWED_ON_NONLEAF",
	0x43 => "LDAP_NOT_ALLOWED_ON_RDN",
	0x44 => "LDAP_ALREADY_EXISTS",
	0x45 => "LDAP_NO_OBJECT_CLASS_MODS",
	0x46 => "LDAP_RESULTS_TOO_LARGE",
	0x47 => "LDAP_AFFECTS_MULTIPLE_DSAS",
	0x50 => "LDAP_OTHER",
	0x51 => "LDAP_SERVER_DOWN",
	0x52 => "LDAP_LOCAL_ERROR",
	0x53 => "LDAP_ENCODING_ERROR",
	0x54 => "LDAP_DECODING_ERROR",
	0x55 => "LDAP_TIMEOUT",
	0x56 => "LDAP_AUTH_UNKNOWN",
	0x57 => "LDAP_FILTER_ERROR",
	0x58 => "LDAP_USER_CANCELLED",
	0x59 => "LDAP_PARAM_ERROR",
	0x5a => "LDAP_NO_MEMORY",
	0x5b => "LDAP_CONNECT_ERROR",
	0x5c => "LDAP_NOT_SUPPORTED",
	0x5d => "LDAP_CONTROL_NOT_FOUND",
	0x5e => "LDAP_NO_RESULTS_RETURNED",
	0x5f => "LDAP_MORE_RESULTS_TO_RETURN",
	0x60 => "LDAP_CLIENT_LOOP",
	0x61 => "LDAP_REFERRAL_LIMIT_EXCEEDED",
	1000 => "Unknown Net_LDAP Error"
	);

	public function __construct($config) {
		include_once('ldap_result.inc.php');
		include_once('ldap_util.inc.php');
		include_once('ldap_schema.inc.php');
		#defino la configuracion:
		$this->_config = $config;
		$this->_setParams($config);
		#objeto util
		//$this->util = ldap_util::getInstance();
	}

	public function __destruct() {
		if ($this->is_connected()) {
			$this->close();
		}
		$this->_connection = null;
		if (defined('DEBUG') && DEBUG == true) {
			log::show();
		}
	}


	# --- funciones especificas a las opciones de la conexion
	#retorna una constante establecida de conexion
	public function getOption($constant) {
		return self::$_options[$constant];
	}

	#establece una constante de conexion
	public function setOption($constant,$value) {
		self::$_options[$constant] = $value;
	}

	#redefine el array de opciones
	public function resetOptions() {
		unset(self::$_options);
		self::$_options = array();
	}


	//retorna la conexion activa:
	public function connection() {
		if ($this->_connection) {
			return $this->_connection;
		} else {
			return false;
		}
	}

	// -- metodos de operacion
	// -- verifica que estamos conectados a la base de datos
	public function is_connected() {
		if ($this->_connected) {
			return true;
		} else {
			return false;
		}
	}

	#alias a las funciones open y close, para base de datos
	public function connect() {
		$this->open();
	}

	#alias de la funcion close
	public function disconnect() {
		$this->close();
	}

	# retorna el mensaje (implementado) del ultimo error ocurrido
	public function error(){
		$errno = ldap_errno($this->_connection);
		$msg = ldap_err2str($errno);
		return $msg;
	}

	//define todos los parametros posibles obtenidos de una DB
	protected function _setParams($config) {
		foreach($config as $key=>$value) {
			switch($key) {
				case 'host':
					$this->_host = $config['host'];
					break;
				case 'username':
					$this->_username = $config['username'];
					break;
				case 'binddn':
					$this->_username = $config['binddn'];
					break;
				case 'password':
					$this->_password = $config['password'];
					break;
				case 'bindpw':
					$this->_password = $config['bindpw'];
					break;
				case 'port':
					if (isset($config['port'])) {
						#otras variables
						$this->_port = $config['port'];
					}
					break;
				case 'options':
					if (is_array($config['options'])) {
						foreach($config['options'] as $k=>$v) {
							$this->setOption($k, $v);
						}
					}
					break;
				case 'basedn':
					$this->_base_dn = $config['basedn'];
				default:
			}
		}
	}

	// --- funciones especificas de LDAP -- //

	#retorna el BASE DN usado en las consultas
	public function base_dn() {
		return $this->_base_dn;
	}

	/**
	 * scope
	 * define el nivel de alcance de la busqueda LDAP
	 *
	 * @param string $scope
	 */
	public function scope($scope = 'sub') {
		$this->_scope = $scope;
	}

	public function sizelimit($size = 0) {
		$this->_sizelimit = (int)$size;
	}

	public function timelimit($seconds = 0) {
		$this->_timelimit = (int)$seconds;
	}

	public function requiredAttributes(array $attr = array()) {
		$this->_onlyneed = $attr;
	}

	public function authenticate($user, $password) {
		return $this->select_user($user, $password);
	}

	// Encode a password for transmission over LDAP
	protected function encode_password($password) {
		$password="\"".$password."\"";
		$encoded="";
		for ($i=0; $i <strlen($password); $i++){
			$encoded.="{$password{$i}}\000";
			}
			return ($encoded);
		}
	}
	?>