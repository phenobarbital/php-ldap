<?php
class ldap {

	const LDAP_DEFAULT = 'default';

	#instancia de la clase
	private static $instance = null;
	#contenedor
	private static $_config = array();

	# -- funcion que inicializa el conector
	/**
	* Funcion base, que inicia a ldap_connections
	*
	* @param string $config_dir
	*/
	public static function init($config_dir = '') {
		define('LDAP_BASE', dirname(__FILE__) . DIRECTORY_SEPARATOR);
		include(LDAP_BASE . 'adapter/ldap.inc.php');
		if (is_dir($config_dir)) {
			$connections = "{$config_dir}ldap_connections.inc.php";
			if (is_file($connections)) {
				#archivo con las conexiones de ldap
				include($connections);
				if (is_array($database)) {
					#itero sobre todas las configuraciones posibles
					foreach($database as $key=>$value) {
						self::$_config[$key] = $value;
					}
				} else {
					ldap::debug('El array database es erroneo o no existe');
				}
			} else {
				ldap::debug('No existe el archivo de configuracion ' . $connections);
			}
		} else {
			ldap::debug('No existe el directorio de configuracion');
		}
	}

	/**
	 * Permite la construccion de un adaptador de acuerdo a las conexiones ya configuradas
	 *
	 * @param string $connection
	 * @return tdbo_adapter
	 */
	public static function load($connection = ldap::LDAP_DEFAULT) {
		//$connection = strtolower($connection);
		#indico cual es el adaptador con quien me voy a conectar
		#defino con que config trabajo por defecto
		$config = self::$_config[$connection];
		#si existe config:
		if ($config) {
			$adapter = $config['adapter'];
			#luego de leidos todos los parametros, configuro el adapter:
			$filename = LDAP_BASE . 'adapter/' . $adapter . DIRECTORY_SEPARATOR . $adapter . '.inc.php';
			if (defined('DEBUG') && DEBUG == true) {
				ldap::debug('LDAP: hemos cargado la configuración para ' . $config['host']);
			}
			if(include_once($filename)) {
				$adapter = new $adapter($config);
				return $adapter;
				if (defined('DEBUG') && DEBUG == true) {
					ldap::debug('LDAP: Se ha creado el conector');
				}
			}
		} else {
			if (defined('DEBUG') && DEBUG == true) {
				ldap::debug('LDAP: No se ha cargado la configuracion');
			}
			return false;
		}
	}

	public static function debug($debug) {
		echo $debug . "\n";
		//log::log($debug . "\n");
	}
}
?>