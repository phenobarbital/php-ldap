#!/usr/bin/php
<?php
include_once 'ldap/ldap.inc.php';
include_once 'file/file.inc.php';

#carga del objeto ldap
ldap::init('conf/');

#base de busqueda en mf:
$basemf = 'OU=Unidades Funcionales,OU=Sede01,OU=MPPF,DC=mf,DC=gov,DC=ve';
$mf = ldap::load('ads');

#base de busqueda en mppf:
$basemppf = 'ou=MF,ou=Usuarios,dc=mf,dc=gob,dc=ve';
$mppf = ldap::load('mppf');

#base de inserción y busqueda en mppef:
$basemppef = 'ou=unidades,dc=mppef,dc=gob,dc=ve';
$mppef = ldap::load('mppef');

#abriendo las conexiones a los 3 servidores
$mf->open();
$mppf->open();
$mppef->open();

#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($mppef);
#construyo el arbol:
ldap_schema::build();
#filtro de busqueda:
$filter = "(&(objectClass=organizationalUnit)(&(!(ou=*Migrado*))(!(ou=Unity))(!(ou=Computador*))(!(ou=Impresora*))(!(ou=Impresoras))(!(ou=Usuarios))(!(ou=Grupos))))";
#primero, verificamos las unidades del MF:
$entries = $mf->query($filter, $basemf, 'base');
/*foreach($entries as $entry) {
	$o = $entry->get_attribute('ou');
	var_dump($o);
	
	/*
	$filtro = "(&(objectClass=organizationalUnit)(ou={$ou}))";
	#si existe la entrada en mppef:
	$e = $mppef->query($filtro, $basemppef, 'base');
	#si la entrada no existe:
	#debo crear la estructura en mppef de la OU:
	if (!$e->count()) {
		#crear una unidad organizacional en mppef:
		$ou = $mppef->create($basemppef);
		$ou->addObjectclass('organizationalUnit');
		#RDN de la entrada:
		$ou->setRDN('');
		#atributo base de la entrada
		$ou->baseAttribute('ou');
		#atributos:
		$ou->ou = $ou;
		$ou->physicalDeliveryOfficeName = $ou;
		$ou->description = $ou;
		$ou->insert();
	}
	*/
//}

/*
#segundo, verificamos las unidades del MPPF:
$entries = $mppf->query($filter, $basemf, 'base');
foreach($entries as $entry) {
	$ou = $entry->ou();
	$filtro = "(&(objectClass=organizationalUnit)(ou={$ou}))";
	#si existe la entrada en mppef:
	$e = $mppef->query($filtro, $basemppef, 'base');
	#si la entrada no existe:
	#debo crear la estructura en mppef de la OU:
	if (!$e->count()) {
		#crear una unidad organizacional en mppef:
		$ou = $mppef->create($basemppef);
		$ou->addObjectclass('organizationalUnit');
		#RDN de la entrada:
		$ou->setRDN('');
		#atributo base de la entrada
		$ou->baseAttribute('ou');
		#atributos:
		$ou->ou = $ou;
		$ou->physicalDeliveryOfficeName = $ou;
		$ou->description = $ou;
		$ou->insert();
	}
}
*/
#cierro las conexiones
$mf->close();
$mppf->close();
$mppef->close();

?>