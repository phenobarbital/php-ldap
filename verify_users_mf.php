#!/usr/bin/php
<?php
include_once 'ldap/ldap.inc.php';
include_once 'file/file.inc.php';

#carga del objeto ldap
ldap::init('conf/');

#base de insercion y busqueda de mf
#inicio la conexion a MF
$basemf = 'OU=Unidades Funcionales,OU=Sede01,OU=MPPF,DC=mf,DC=gov,DC=ve';
$mf = ldap::load('ads');
$mf->open();

#filtro de busqueda para MF:
$filter = "(&(objectClass=user)(samaccounttype=". ADS_NORMAL_ACCOUNT ."))";
#primero, verificamos las unidades del MF:
$entries = $mf->query($filter, $basemf, 'sub');
$usersmf = array();
foreach($entries as $entry) {
    $usersmf[strtolower($entry->sAMAccountName)] = $entry->name;
}

#base de insercion y busqueda de mppf
#inicio la conexion a MF
$basemf = 'ou=MF,ou=Usuarios,dc=mf,dc=gob,dc=ve';
$filtermppf = "(&(objectClass=posixAccount)(uid=*))";
$mppf = ldap::load('mppf');
$mppf->open();
$usersmppf = array();
$b = $mppf->query($filtermppf, $basemppf, 'sub');
foreach($b as $entry) {
    $usersmppf[$entry->uid] = $entry->cn;
}

#base de inserción y busqueda en mppef:
$basemppef = 'ou=unidades,dc=mppef,dc=gob,dc=ve';
$filtermppef = "(&(objectClass=posixAccount)(uid=*))";
$mppef = ldap::load('mppef');
$mppef->open();
$usersmppef = array();
$c = $mppef->query($filtermppef, $basemppef, 'sub');
foreach($c as $entry) {
    $usersmppef[strtolower($entry->uid)] = $entry->cn;
}
#diferencia
$total = array_diff_key($usersmf, $usersmppef);
echo '<pre>';
print_r($total);
echo '</pre>';
echo '<br />';

echo '<pre>';
echo "diferencia total entre MF y MPPEF: " . count($total);
echo '</pre>';

#diferencia
$total = array_diff_key($usersmppf, $usersmppef);
echo '<pre>';
print_r($total);
echo '</pre>';

echo '<pre>';
echo "diferencia total entre MPPF y MPPEF: " . count($total);
echo '</pre>';

#cerrar conexiones
$mf->close();
$mppef->close();
?>