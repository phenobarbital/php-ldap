#!/usr/bin/php
<?php
include "conf/base.inc.php";
include BASE_DIR . "conf/include_ldap.inc.php";

//#inicio la conexion a MF
//$basemf = 'OU=Unidades Funcionales,OU=Sede01,OU=MPPF,DC=mf,DC=gov,DC=ve';
//$mf = ldap::load('ads');
//$mf->open();

#base de busqueda en alucasa:
$basead = 'DC=PRODUCCION,DC=com';
$ad = ldap::load('active');
$ad->open();

#base de inserción y busqueda en mppef:
$basealucasa = 'dc=alucasa,dc=com,dc=ve';
$alucasa = ldap::load('ldap');

#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($alucasa);
#construyo el arbol:
ldap_schema::build();

#Filtro de busqueda de grupos.
$filter = "(objectClass=group)";
$entries = $ad->query($filter, $basead, 'sub');

foreach($entries as $entry) {
	#Crear un grupo
	$gid = $alucasa->create($basealucasa);
	#Agrego los objectclasses efectivos de una entrada:
	$gid->addObjectclass('posixGroup');
	$gid->addObjectClass('sambaGroupMapping');
	#atributo base de la entrada
	$gid->baseAttribute('cn');
	#atributos que no cambian:
	$gid->cn = $entry->get_attribute('cn');
	//$gid->description = $entry->get_attribute('description');
	//$gid->sambaGroupType = $entry->get_attribute('groupType');
	$gid->sambaGroupType = '2';
	$objsid = $entry->bin_to_str_sid('objectSid');
	$gid->sambaSID = $objsid;
	#miembros
	$miembros = array();
	if(is_array($entry->member)) {
		foreach($entry->member as $member) {
			$a = explode(',', $member);
			$f = "({$a[0]})";
			$u = $ad->query($f, $basead, 'sub');
			$miembros[] = $u->sAMAccountName;
		}
	}
	$gid->memberUid = $miembros;
	#GID:
	#domain sid
	$sid = 'S-1-5-21-89404532-1372478125-1516182889-';
	$gid->gidNumber = str_replace($sid, '', $objsid);
	#por ultimo, extraer la unidad funcional a la que pertenece:
	$a = ldap_explode_dn($entry->dn(), 1);
	unset($a['count']);
	$b = ldap_explode_dn($basead, 1);
	unset($b['count']);
	$ou = array_diff($a, $b);
	#RDN de la entrada:
	if ($ou[1]!='Builtin') {
		if ($ou[1]!='') {
			if ($ou[1] == 'Users') {
				$gid->setRDN("cn=Grupos");
			} else {
				$gid->setRDN("ou={$ou[1]}");
			}
		}
	}
	#
	$filter = "(&(objectClass=group)(CN={$entry->cn}))";
	$a = $ad->query($filter, $basead, 'sub');
	if ($a->dn()!='') {
		$gid->save();
	} else {
		$gid->insert();
	}
}

$ad->close();
$alucasa->close();
?>