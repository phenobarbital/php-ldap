#!/usr/bin/php
<?php
include "conf/base.inc.php";
include BASE_DIR . "conf/include_ldap.inc.php";

#carga del objeto ldap

#base de busqueda en alucasa:
$basead = 'DC=PRODUCCION,DC=com';
$ad = ldap::load('active');
$ad->open();

#base de inserción y busqueda en mppef:
$basealucasa = 'cn=Usuarios,dc=alucasa,dc=com,dc=ve';
$alucasa = ldap::load('ldap');

#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($alucasa);
#construyo el arbol:
ldap_schema::build();

#creo mi archivo de log de usuarios malos:
$file = new file();
$file->filename('usersalucasabad.ldif');
$file->open();

if ($alucasa->open()) {
	#busco los usuarios:
	#filtro de busqueda:
	$filter = "(&(objectClass=user)(samaccounttype=". ADS_NORMAL_ACCOUNT .")(samaccountname=*))";
	//$filter = "(&(objectClass=user)(samaccountname=*))";
	#primero, verificamos las unidades del MF:
	$entries = $ad->query($filter, $basead, 'sub');
	//var_dump($entries->attributes());
	$i = 0;
	foreach($entries as $entry) {
		#Crear un usuario
		$uid = $alucasa->create($basealucasa);
		#Agrego los objectclasses efectivos de una entrada:
		$uid->addObjectclass('organizationalPerson');
		$uid->addObjectClass('inetOrgPerson');
		$uid->addObjectClass('sambaSamAccount');
		$uid->addObjectClass('qmailUser');
		$uid->addObjectClass('posixAccount');
		$uid->addObjectClass('shadowAccount');
		#atributo base de la entrada
		$uid->baseAttribute('cn');
		#atributos que cambian:
		$objsid = $entry->bin_to_str_sid('objectSid');
		$user = strtolower($entry->get_attribute('sAMAccountName'));
		$name = mb_convert_encoding($entry->get_attribute('displayName'), 'utf8');
		if ($name) {
			$uid->displayName = $name;
			$uid->gecos = $name;
		}
		# password
		$uid->userPassword = '{SSHA}eKPVANWpnHz4wJieFjVTooi5LunKEAVW';
		#atributos que no cambian:
		$ou = $entry->get_attribute('physicalDeliveryOfficeName');
		if ($ou) {
			$uid->physicalDeliveryOfficeName = $ou;
		}
		$department = str_replace(',', '', $entry->get_attribute('department'));
		if ($department) {
			$uid->ou = $department;
		}
		$company = str_replace(',', '', $entry->get_attribute('company'));
		if ($company) {
			$uid->o = $company;
		}
		$givenName = $entry->get_attribute('givenName');
		if ($givenName) {
			$uid->givenName = $givenName;
		}
		$uid->displayName = $name;
		$cn = $entry->get_attribute('cn');
		$uid->cn = $cn;
		$sn = $entry->get_attribute('sn');
		if (!$sn) {
			$uid->sn = $cn;
		} else {
			$uid->sn = $sn;
		}
		
		$tlf = $entry->get_attribute('telephoneNumber');
		if ($tlf) {
			$uid->telephoneNumber = $tlf;
		}
		$title = $entry->get_attribute('title');
		if($title) {
			$uid->title = $title;
		}
		$initials = $entry->get_attribute('initials');
		if ($initials) {
			$uid->initials = $initials;
		}
		$postal = $entry->get_attribute('postalCode');
		if ($postal) {
			$uid->postalCode = $postal;
		}
		#cuenta de correo alucasa:
		$user = mb_convert_encoding($user, 'utf8');
		$uid->mail = "{$user}@alucasa.com.ve";

		#uid y cn del usuario:
		$uid->uid = strtolower($user);
		//$cn = mb_convert_encoding($entry->get_attribute('cn'), 'utf8');

		#informacion:
		$uid->sambaSID = $objsid;

		$comment = $entry->get_attribute('comment');
		if ($comment) {
			$uid->description = $comment;
		}

		$lastLogon = $entry->get_attribute('lastLogon');
		if ($lastLogon) {
			$uid->sambaLogonTime = $lastLogon;
		}
		$id = $entry->get_attribute('primaryGroupID');
		if ($id) {
			$uid->sambaPrimaryGroupSID = $id;
		}
		$pwd = $entry->get_attribute('pwdLastSet');
		if ($pwd) {
			$uid->sambaPwdLastSet = $pwd;
		}
		$description = $entry->get_attribute('description');
		if ($description) {
			$uid->description = $description;
		}
		$script = $entry->get_attribute('scriptPath');
		if($script) {
			$uid->sambaLogonScript = $script;
		}
		#UID y GID:
		#domain sid
		//$sid = 'S-1-5-21-1658329406-718221906-1845911597-';
		//$uid->uidNumber = str_replace($sid, '', $objsid);
		$uid->uidNumber = str_replace('S-1-5-21-89404532-1372478125-1516182889-', '', $objsid);
		#pertenece by default a domain users
		$uid->gidNumber = '513';
		$uid->homeDirectory = "/home/{$user}";
		$uid->loginShell = '/bin/bash';
		$uid->setRDN('');
		/*
		 #direcciones de correo alternativas:
		 #la cuenta esta en mail:
		 $uid->mailHost = 'mail.alucasa.com.ve';
		 //$uid->mailAlternateAddress = array("{$user}@mf.gov.ve", "{$user}@mppf.gob.ve", "{$user}@mf.gob.ve");
		 #por ultimo, extraer la unidad funcional a la que pertenece:
		 $a = ldap_explode_dn($entry->dn(), 1);
		 unset($a['count']);
		 $b = ldap_explode_dn($basead, 1);
		 unset($b['count']);
		 $ou = array_diff($a, $b);
		 #RDN de la entrada:

		 #atributo que indica a que unidad pertenece:
		 $uid->ou = $ou[2];
		 */
		#atributos especificos de miraflores:
		$uid->sambaDomainName = 'ALUCASA.COM.VE';
		$uid->accountStatus = 'active';
		$uid->deliveryMode = 'virtual:';
		$uid->mailQuotaSize = 524288;
		# atributos del samba
		$uid->sambaAcctFlags = '[U          ]';
		if($entry->get_attribute('lastLogon')) {
			$uid->sambaLogonTime = $entry->get_attribute('lastLogon');
		}
		$uid->sambaPwdMustChange = $entry->get_attribute('accountExpires');
		$uid->sambaLogoffTime = '2147483647';
		$uid->sambaKickoffTime = '2147483647';
		$uid->sambaPwdLastSet = $entry->get_attribute('pwdLastSet');
		$uid->sambaPwdCanChange = '2147483647';
		$uid->sambaNTPassword = '';
		#crear:
		if (!$uid->insert()) {
			print_r($uid->row());
			$file->write("\n Error insertando a: " . $name . "\n");
			$file->write($entry->toLDIF());
			#agregamos un salto de linea al final de cada ldif:
			$file->write("\n");
			$i++;
		}
	}
	echo 'Se importaron ' . $entries->count() . " usuarios.\n";
	echo "Hubo {$i} errores de insercion; revisar log\n";
	$ad->close();
	$alucasa->close();
}

#cerramos el archivo
$file->close();
?>