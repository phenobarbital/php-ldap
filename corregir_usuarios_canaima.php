#!/usr/bin/php
<?php
include "conf/base.inc.php";
include BASE_DIR . "conf/include_ldap.inc.php";

#carga del objeto ldap
#base de inserción y busqueda en canaima:
$basecanaima = 'ou=gente,ou=plataforma,o=canaima';
$canaima = ldap::load('canaima');

#cargar los schemas de mppef (para razones de creacion y validacion de entradas)
#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($canaima);
#construyo el arbol:
ldap_schema::build();

//#creo mi archivo de log de usuarios malos:
//$file = new file();
//$file->filename('canaimabad.ldif');
//$file->open();

if ($canaima->open()) {
	#filtro de busqueda:
	$filter = "(&(objectClass=tracUser)(tracperm=WIKI_VIEW)(uid=*))";
	#primero, verificamos las unidades de Canaima
	$entries = $canaima->query($filter, $basecanaima, 'sub');
	$i = 0;
	foreach($entries as $entry) {
		$entry->baseAttribute('uid');
		$entry->setRDN('');
		$entry->tracperm = 'none';
		$entry->employeeType = 'usuario';
		#actualizar:
		if (!$entry->save()) {
			print_r($entry->dn());
			$i++;
		}
	}
	echo 'Se importaron ' . $entries->count() . " usuarios.\n";
	echo "Hubo {$i} errores de modificacion; revisar log\n";
	$canaima->close();
}
?>