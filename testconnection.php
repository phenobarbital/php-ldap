#!/usr/bin/php
<?php
include "conf/base.inc.php";
include BASE_DIR . "conf/include_ldap.inc.php";

//ldap::debug("* iniciando script de pruebas ldap");

# cargamos el adapador
$cantv = ldap::load('ldap');

// atrimos la conexion
$cantv->open();

#objeto unico dentro de todo el arbol LDAP
ldap_schema::setAdapter($cantv);
#construyo el arbol:
ldap_schema::build();

//var_dump(ldap_schema::get_objectclass('krbPrincipalAux'));

$filtro = '(objectClass=sambaSamAccount)';
$entry = $cantv->query($filtro);

foreach($entry as $v) {
	var_dump($v->toLDIF());
}

#cerramos la conexion LDAP
$cantv->close();