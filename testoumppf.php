#!/usr/bin/php
<?php
include_once 'ldap/ldap.inc.php';
include_once 'file/file.inc.php';

ldap::init('conf/');
$ldap = ldap::load('mppf');

$ldap->open();
$base = 'ou=MF,ou=Usuarios,dc=mf,dc=gob,dc=ve';
$filter = "(&(objectClass=organizationalUnit)(&(!(ou=*Migrado*))(!(ou=Unity))(!(ou=Computador*))(!(ou=Impresora*))(!(ou=Impresoras))(!(ou=Usuarios))(!(ou=Grupos))))";
$entry = $ldap->query($filter, $base, 'base');
//itera sobre todas las entradas de tipo computadora y las guarda en un LDIF:
$file = new file();
$file->filename('ldifsoumppf.ldif');
#abrimos el archivo:
$file->open('w');
foreach($entry as $v) {
	//echo $entry->cn(), ": ", $entry->bin_to_str_sid('objectSid'), "\n";
	$file->write($v->toLDIF());
	echo $v->dn(), "\n";
	#agregamos un salto de linea al final de cada ldif:
	$file->write("\n");
}
#numero de grupos:
echo "Existen : ", $entry->num_rows(), " unidades funcionales en MPPF.\n";

#cerramos el archivo
$file->close();

#y la conexion LDAP
$ldap->close();
?>