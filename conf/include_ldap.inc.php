<?php
/*
 * instalar para usar:
 * aptitude install php5-snmp php5-mcrypt php5-curl php5-common php5-auth-pam php5-ldap php5-cli
 */
include BASE_DIR . 'ldap/ldap.inc.php';
include BASE_DIR . 'samba/lm.php';

# la base de todas las busquedas
define('BASE_DN', 'dc=cantv,dc=com,dc=ve');
#carga del objeto ldap
ldap::init(BASE_DIR . 'conf/');
?>