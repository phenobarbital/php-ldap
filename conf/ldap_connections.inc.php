<?php
  #una conexion usando el conector ldap
  $database['ldap']['adapter'] 		= 'openldap';
  $database['ldap']['host'] 		= '172.22.2.170';
  $database['ldap']['basedn'] 		= 'dc=alucasa,dc=com,dc=ve';
  $database['ldap']['binddn'] 	= 'cn=admin,dc=alucasa,dc=com,dc=ve';
  $database['ldap']['bindpw'] 	= 'testing';
  $database['ldap']['port'] 		= 389;
  $options = array('LDAP_OPT_PROTOCOL_VERSION' => 3, 'LDAP_OPT_REFERRALS'=>0, 'LDAP_OPT_SIZELIMIT'=>5000, 'LDAP_OPT_TIMELIMIT'=>1000);
  $database['ldap']['options'] 		= $options; 
  
    #una conexion usando un Active Directory Service
  $database['active']['adapter'] 		= 'ads';
  $database['active']['host'] 		= '172.22.2.161';
  $database['active']['basedn'] 		= 'DC=PRODUCCION,DC=com';
  $database['active']['domain']		= 'produccion.com';
  $database['active']['netbios_name']	= 'PRODUCCION';
  $database['active']['username'] 		= 'Administrator';
  $database['active']['password'] 		= 'testing';
  $database['active']['port'] 		= 389;
  $options = array('LDAP_OPT_PROTOCOL_VERSION' => 3, 'LDAP_OPT_REFERRALS'=>0, 'LDAP_OPT_SIZELIMIT'=>5000, 'LDAP_OPT_TIMELIMIT'=>300);
  $database['active']['options'] 		= $options;

?>
