<?php
define('DS', DIRECTORY_SEPARATOR);
//El app_dir es descubierto, pero puede ser indicado por el usuario
$app_dir = pathinfo(dirname(__FILE__), PATHINFO_DIRNAME);
//directorio principal de la aplicacion (por lo general, no necesita cambiar esta directiva):
define('BASE_DIR', $app_dir . DS);

include BASE_DIR . 'log/log.inc.php';
include BASE_DIR . 'file/file.inc.php';
//ver errores:
define('DEBUG', true);
?>